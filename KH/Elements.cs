﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KH
{
    public class Elements
    {
        public class Registrations
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string RGNR { get; set; }
            public string RGDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string CLAS { get; set; }
            public string EXDA { get; set; }
            public string PGNR { get; set; }
        }

        public class Renewals
        {
            public static string APNR { get; set; }
            public static string APDA { get; set; }
            public static string RGNR { get; set; }
            public static string RGDA { get; set; }
            public static string CLAS { get; set; }
            public static string DESC { get; set; }
        }
    }
}

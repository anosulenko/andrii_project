﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KH
{
    class KH_main
    {
        public static DirectoryInfo PathToTetml = new DirectoryInfo(@"C:\Users\Razrab\Desktop\TET");
        public static FileInfo currentFile = null;
        static void Main(string[] args)
        {
            var files = new List<string>();
            foreach (FileInfo file in PathToTetml.GetFiles("*.tetml", SearchOption.AllDirectories))
                files.Add(file.FullName);

            XElement elem = null;
            List<XElement> eRegList = new List<XElement>(); // registrations elements
            List<XElement> eRenList = new List<XElement>(); // renewals elements
            List<XElement> imageList = new List<XElement>(); //images elements
            Dictionary<string, string> Images = new Dictionary<string, string>();

            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                elem = XElement.Load(file);
                eRegList = elem.Descendants().Where(e => e.Name.LocalName == "Text" || e.Name.LocalName == "PlacedImage")
                    .ToList();
                imageList = elem.Descendants().Where(i => i.Name.LocalName == "Image")
                    .ToList();

                foreach (var image in imageList)
                {
                    var value = image.Attribute("id")?.Value;
                    if (value != null)
                    {
                        var value1 = image.Attribute("filename")?.Value;
                        Images.Add(value, image.Attribute("filename")?.Value);
                    }
                }

                if (eRegList.Count > 0)
                {
                    var processedRecords = Processing.Registrations(eRegList, Images);
                    Output.RegistrationToFile(processedRecords, Processing.processed);
                }
                else
                    Console.WriteLine("Reg elements was not found");
            }
        }
    }
}

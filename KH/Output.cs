﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static KH.Elements;

namespace KH
{
    public class Output
    {
        public static void RegistrationToFile(List<Registrations> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Reg.txt");
            var sf = new StreamWriter(path);
            if(output != null)
            {
                foreach(var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("RGNR:\t" + record.RGNR);
                        sf.WriteLine("RGDA:\t" + record.RGDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("CORN:\t" + record.CORN);
                        sf.WriteLine("CORA:\t" + record.CORA);
                        sf.WriteLine("CORC:\t" + record.CORC);
                        sf.WriteLine("CLAS:\t" + record.CLAS);
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace KH
{
    class Processing : Elements
    {
        private static readonly string I1 = "1-";
        private static readonly string I2 = "2-";
        private static readonly string I3 = "3-";
        private static readonly string I4 = "4-";
        private static readonly string I5 = "5-";
        private static readonly string I6 = "6-";
        private static readonly string I7 = "7-";
        private static readonly string I8 = "8-";
        private static readonly string I9 = "9-";
        private static readonly string I10 = "10-";
        private static readonly string I11 = "11-";
        private static readonly string I12 = "12-";

        public static DirectoryInfo processed;
        public static List<Registrations> Registrations(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Registrations> elementsOut = new List<Registrations>();

            if(elements != null && elements.Count > 0)
            {
                string[] splittedRecord = null;
                string tmpRecordValue;
                int tmpInc;
                for(int i = 0; i < elements.Count; ++i)
                {
                    var value = elements[i].Value;
                    if (value.StartsWith(I1))
                    {
                        var currentElement = new Registrations();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            tmpRecordValue += elements[tmpInc].Value + "\n";
                            ++tmpInc;
                        } while (tmpInc < elements.Count() && !elements[tmpInc].Value.StartsWith(I1));
                        if(tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }

                        foreach(var record in splittedRecord)
                        {
                            if (record.StartsWith(I1))
                            {
                                currentElement.APNR = record.Replace(I1, "").Trim().Replace(" /", "_");
                                if (lastImageId != null)
                                {
                                    currentElement.TMTY = "M";
                                    
                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(KH_main.PathToTetml.FullName, Images[lastImageId]);
                                    processed = Directory.CreateDirectory(Path.Combine(KH_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(KH_main.currentFile.FullName) + "\\App"));
                                    if (File.Exists(fileName))
                                    {
                                        try
                                        {
                                            File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR) + ext);
                                        }
                                        catch (Exception)
                                        {
                                            Console.WriteLine("Image already exist:\t" + fileName);
                                        }
                                    }
                                    else
                                    {
                                        currentElement.TMTY = "W";
                                    }
                                }
                            }
                            if (record.StartsWith(I2))
                            {
                                currentElement.APDA = Methods.DateNormalize(record.Replace(I2, "").Trim());
                            }
                            if (record.StartsWith(I3))
                            {
                                currentElement.OWNN = record.Replace(I3, "").Trim();
                            }
                            if (record.StartsWith(I4))
                            {
                                var split = record.Split(',');
                                currentElement.OWNA = record.Replace(I4, "").Replace(split.Last(), "").Trim(',').Trim().Replace("\n", " ");
                            }
                            if (record.StartsWith(I5))
                            {
                                currentElement.OWNC = Methods.ToOWNC(record.Replace(I5, "").Trim());
                            }
                            if (record.StartsWith(I6))
                            {
                                currentElement.CORN = record.Replace(I6, "").Trim();
                            }
                            if (record.StartsWith(I7))
                            {
                                var split = record.Split(',');
                                currentElement.CORA = record.Replace(I7, "").Replace(split.Last(), "").Trim(',').Trim().Replace("\n", " ");
                                var CORC = split.Last().Trim();
                                CORC = Methods.ToCORC(CORC);
                                currentElement.CORC = CORC;
                            }
                            if (record.StartsWith(I8))
                            {
                                currentElement.RGNR = record.Replace(I8, "").Trim();
                            }
                            if (record.StartsWith(I9))
                            {
                                currentElement.RGDA = Methods.DateNormalize(record.Replace(I9, "").Trim());
                            }
                            if (record.StartsWith(I10))
                            {
                                currentElement.TMNM = record.Replace(I10, "").Trim();
                            }
                            if (record.StartsWith(I11))
                            {
                                currentElement.CLAS = record.Replace(I11, "").Trim();
                            }
                            if (record.StartsWith(I12))
                            {
                                var date = Regex.Match(record, "[0-9]{2}/[0-9]{2}/[0-9]{4}");
                                currentElement.EXDA = Methods.DateNormalize(date.Value);
                            }
                        }
                    }
                }
            }
            
            return elementsOut;
        }

        public static List<Renewals> Renewals(List<XElement> e)
        {
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KH
{
    public class Methods
    {
        public static string[] RecSplit(string recString)
        {
            string[] splittedRecord = null;
            string tempStrC = recString.Replace("\n", " ").Replace("__________________________________", "").Trim();

            if (tempStrC != "")
            {
                if (tempStrC.Contains("-"))
                {
                    Regex regexPatOne = new Regex(@" \d\d?-\s", RegexOptions.IgnoreCase);
                    MatchCollection matchesClass = regexPatOne.Matches(tempStrC);
                    if (matchesClass.Count > 0)
                    {
                        foreach (Match matchC in matchesClass)
                        {
                            tempStrC = tempStrC.Replace(matchC.Value, "***" + matchC.Value);
                        }
                    }
                }
                /*Splitting record*/
                splittedRecord = tempStrC.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            }
            return splittedRecord;
        }




        public static string DateNormalize(string s)
        {
            var split = s.Split('/');
            s = split[2] + split[1] + split[0];

            return s;
        }

        public static string ToCORC(string s)
        {
            if (s == "Cambodia")
                s = "KH";
            if (s == "Japan")
                s = "JP";

            if(s != "JP" && s != "KH")
            {
                s = null;
            }

            return s;
        }

        public static string ToOWNC(string s)
        {
            switch (s)
            {
                case "Thailand":
                    s = "TH";
                    break;
                case "Cambodia":
                    s = "KH";
                    break;
                case "China":
                    s = "CN";
                    break;
                case "Singapore":
                    s = "SG";
                    break;
                case "Malaysia":
                    s = "MY";
                    break;
                case "Indonesia":
                    s = "ID";
                    break;
                case "Hong Kong":
                    s = "HK";
                    break;
                case "Switzerland":
                    s = "CH";
                    break;
                case "Republic of Korea":
                    s = "KR";
                    break;
                case "United Kingdom":
                    s = "GB";
                    break;
                case "Republic of Mauritius":
                    s = "MU";
                    break;
                case "United States of America":
                    s = "US";
                    break;
                case "R.O.C.":
                    s = "TW";
                    break;
                case "Japan":
                    s = "JP";
                    break;
                case "United Arab Emirates":
                    s = "AE";
                    break;
                case "India":
                    s = "IN";
                    break;
                case "France":
                    s = "FR";
                    break;
                case "Vietnam":
                    s = "VN";
                    break;
                case "British Virgin Islands":
                    s = "TH";
                    break;
                case "The Netherlands":
                    s = "NL";
                    break;
                case "Ireland":
                    s = "IE";
                    break;
                case "Cayman Islands":
                    s = "KY";
                    break;
                case "Germany":
                    s = "DE";
                    break;
                case "Sweden":
                    s = "SE";
                    break;
            }

            return s;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace AO
{
    class Processing : Elements
    {
        private static List<string> tmpAPNR;

        private static readonly string I210 = "(210)";
        private static readonly string I730 = "(730)";
        private static readonly string I220 = "(220)";
        private static readonly string I300 = "(300)";
        private static readonly string I511 = "(511)";
        private static readonly string I540 = "(540)";

        public static DirectoryInfo processed;

        public static List<Applications> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if(elements != null && elements.Count > 0)
            {
                string[] splittedRecord = null;
                string tmpRecordValue;
                int tmpInc;
                int pgnr = 1;
                for (int i = 0; i < elements.Count; i++)
                {
                    if(elements[i].Name.LocalName == "Page")
                    {
                        pgnr = Methods.ToInt(elements[i].Attribute("number").Value);
                    }
                    var value = elements[i].Value.Replace("_","").Trim();
                    if (value.StartsWith(I210))
                    {
                        var currentElement = new Applications();
                        string lastImageId = null;
                        tmpAPNR = new List<string>();

                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Replace("_", "").Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.Replace("_", "").Trim().StartsWith(I210));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }

                        foreach(var record in splittedRecord)
                        {
                            if (record.StartsWith(I210))
                            {
                                if (string.IsNullOrEmpty(currentElement.APNR))
                                {
                                    var tmpRec = record.Trim().Replace(I210, "").Trim();
                                    var APNR = Regex.Matches(tmpRec, @"\d+");
                                    if (APNR.Count() == 1)
                                    {
                                        currentElement.APNR = tmpRec.Replace(I210, "").Replace("(", "").Trim();
                                    }
                                    else
                                    {
                                        currentElement.APNR = APNR.First().ToString().Replace(I210, "").Trim();
                                        foreach(Match match in APNR)
                                        {
                                            tmpAPNR.Add(match.Value);
                                        }
                                    }

                                    if(lastImageId != null)
                                    {
                                        string ext = Path.GetExtension(Images[lastImageId]);
                                        string fileName = Path.Combine(AO_main.PathToTetml.FullName, Images[lastImageId]);
                                        processed = Directory.CreateDirectory(Path.Combine(AO_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(AO_main.currentFile.FullName) + "\\App"));
                                        if (File.Exists(fileName))
                                            foreach(var item in tmpAPNR)
                                            {
                                                try { File.Copy(fileName, Path.Combine(processed.FullName, item) + ext); }
                                                catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                            }
                                        else Console.WriteLine("Cannot locate file " + fileName);
                                    }
                                }
                            }
                            if (record.StartsWith(I220))
                            {
                                var APDA = Regex.Match(record, @"\d{2}-\d{2}-\d{4}");
                                currentElement.APDA = Methods.DateNormalize(APDA.Value);
                            }
                            if (record.StartsWith(I730))
                            {
                                var matchOWNN = Regex.Match(record, @".*?\.\n", RegexOptions.Singleline);
                                var OWNN = matchOWNN.Value.Replace("\n", " ");
                                if (string.IsNullOrEmpty(OWNN))
                                {
                                    OWNN = record.Split('\n').First();
                                }
                                var OWNA = record.Replace("\n", " ").Replace(OWNN, "");
                                var OWNC = Methods.ToOWNC(OWNA);
                                currentElement.OWNN = OWNN.Replace(I730, "").Trim();
                                currentElement.OWNA = OWNA.Trim();
                                currentElement.OWNC = OWNC;
                            }
                            if (record.StartsWith(I300))
                            {
                                var tmpValue = record.Replace(I300, "").Replace("\n", " ").Trim();
                                var pattern = new Regex(@"(?<Date>\d{2}\-\d{2}\-\d{4})\s(?<Country>[^\d]+)(?<Number>\d.*?)\s.*");
                                var match = pattern.Match(tmpValue);
                                if (match.Success)
                                {
                                    currentElement.PRID = Methods.DateNormalize(match.Groups["Date"].Value.Trim());
                                    currentElement.PRIC = Methods.ToPRIC(match.Groups["Country"].Value.Trim());
                                    currentElement.PRIN = match.Groups["Number"].Value;
                                }
                            }
                            if (record.StartsWith(I511))
                            {
                                List<string> classes = new List<string>();
                                List<string> descs = new List<string>();

                                var input = record.Replace(I511, "");
                                var pattern = new Regex(@"(?<CLAS>\d{2}\s).{0,3}(?<DESC>[^0-9]+)");
                                var matches = pattern.Matches(input);

                                foreach(Match item in matches)
                                {
                                    var @class = Regex.Match(item.Value, "[0-9]{1,2} ");
                                    classes.Add(@class.Value.Trim());
                                    descs.Add(item.Value.Replace(@class.Value, "").Replace("\n", " ").Trim(' ', '–', ' ', '-', ' '));
                                }

                                currentElement.CLAS = classes;
                                currentElement.DESC = descs;
                            }
                            if (record.StartsWith(I540))
                            {
                                currentElement.TMNM = record.Replace(I540, "");
                                currentElement.TMTY = "M";
                            }
                            currentElement.PGNR = $"{pgnr}";
                        }
                        if (tmpAPNR.Count > 0)
                        {
                            elementsOut.Add(currentElement);
                            for (int j = 1; j < tmpAPNR.Count; j++)
                            {
                                var newCurrentElement = (Applications)currentElement.Clone();
                                newCurrentElement.APNR = tmpAPNR[j];
                                elementsOut.Add(newCurrentElement);
                            }
                        }
                        else
                        {
                            elementsOut.Add(currentElement);
                        }
                    }
                }
            }
            return elementsOut;
        }
    }

    public interface ICloneable
    {
        object Clone();
    }
}

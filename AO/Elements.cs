﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AO
{
    public class Elements
    {
        public class Applications : ICloneable
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string PRIC { get; set; }
            public string PRIN { get; set; }
            public string PRID { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string PGNR { get; set; }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }
    }
}

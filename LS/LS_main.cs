﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace LS
{
    class LS_main
    {
        public static DirectoryInfo PathToTetml = new DirectoryInfo(@"C:\Users\Razrab\Desktop\LS\");
        public static FileInfo currentFile = null;
        public static DirectoryInfo processed = null;
        public static string currentFileName = null;
        static void Main(string[] args)
        {
            var files = new List<string>();
            foreach (FileInfo file in PathToTetml.GetFiles("*.htm", SearchOption.AllDirectories))
                files.Add(file.FullName);

            XElement elem = null;
            List<XElement> eRegList = new List<XElement>();
            List<XElement> imageList = new List<XElement>();
            Dictionary<string, string> Images = new Dictionary<string, string>();

            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                currentFileName = Path.GetFileName(file);
                var text = File.ReadAllText(file);

                if (!string.IsNullOrEmpty(text))
                {
                    if (currentFileName.Contains("Applications"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName) + "\\App"));
                        var processedRecords = Processing.Applications(text, Images);
                        Output.ApplicationToFile(processedRecords, processed);
                    }
                    else if (currentFileName.Contains("Ownerchange"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName) + "\\Ownerchange"));
                        var processedRecords = Processing.OwnerChange(text, Images);
                        Output.OwnerchangeToFile(processedRecords, processed);
                    }
                    else if (currentFileName.Contains("Registrations"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName) + "\\Reg"));
                        var processedRecords = Processing.Registrations(text, Images);
                        Output.RegistrationToFile(processedRecords, processed);
                    }
                    //var processedRecords = Processing.Applications(text, Images);
                    //Output.ApplicationToFile(processedRecords, processed);
                    //Output.ApplicationToFile(processedRecords, Processing.processed);
                }
                else
                    Console.WriteLine("Reg elements was not found");
            }
        }
    }
}

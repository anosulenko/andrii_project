﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LS
{
    class Methods
    {
        public static string[] RecString(string input)
        {
            string[] splittedRecords;

            var fields = Regex.Matches(input, @"\(\d{3}\)");

            foreach(Match field in fields)
            {
                input = input.Replace(field.Value, $"***{field}");
            }

            splittedRecords = input.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();

            return splittedRecords;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            var date = Regex.Match(s, @"(?<day>\d{2})[^0-9]*(?<month>\d{2})[^0-9]*(?<year>\d{4})");
            dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            return dateNormalized;
        }
    }
}

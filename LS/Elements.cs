﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LS
{
    public class Elements
    {
        public class Applications
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string PGNR { get; set; }
        }

        public class Ownerchange
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string PGNR { get; set; }
        }

        public class Registrations
        {
            public string APNR { get; set; }
            public string RGNR { get; set; }
            public string RGDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string NOTE { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace LS
{
    class Processing : Elements
    {
        private static readonly string I220 = "(220)";
        private static readonly string I210 = "(210)";
        private static readonly string I540 = "(540)";
        private static readonly string I731 = "(731)";
        private static readonly string I740 = "(740)";
        private static readonly string I511 = "(511)";
        private static readonly string I151 = "(151)";
        private static readonly string I111 = "(111)";

        public static DirectoryInfo processed;

        public static List<Applications> Applications(string input, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if (!string.IsNullOrEmpty(input))
            {
                string[] splittedRecords = null;
                string tmpRecordValue;
                int tmpInc;
                var elements = input.Split('\n');
                for(int i = 0; i < elements.Length; i++)
                {
                    if (elements[i].StartsWith(I220))
                    {
                        var currentElement = new Applications();
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            tmpRecordValue += elements[tmpInc] + "\n";
                            ++tmpInc;
                        } while (tmpInc < elements.Length && !elements[tmpInc].StartsWith(I220));

                        if (tmpRecordValue != null)
                            splittedRecords = Methods.RecString(tmpRecordValue);

                        foreach(var record in splittedRecords)
                        {
                            if (record.StartsWith(I210))
                            {
                                currentElement.APNR = record.Replace(I210, "").Replace("/", "_").Trim();
                            }
                            if (record.StartsWith(I220))
                            {
                                currentElement.APDA = Methods.DateNormalize(record.Replace(I220, "").Trim());
                            }
                            if (record.StartsWith(I540))
                            {
                                currentElement.TMTY = "M";
                                var image = record.Replace(I540, "").Trim();
                                if (!string.IsNullOrEmpty(image))
                                {
                                    currentElement.TMNM = image;
                                    currentElement.TMTY = "W";
                                }
                            }
                            if (record.StartsWith(I731))
                            {
                                var text = record.Replace(I731, "").Trim();
                                var OWNC = Regex.Match(text, @"(\p{Lu}\p{Ll}+((-|\s)\p{L}+)?)$").Value;
                                if (string.IsNullOrEmpty(OWNC))
                                {
                                    OWNC = "U.S.A";
                                }
                                var index = text.IndexOf("of");
                                var OWNA = text.Substring(index + 2);
                                var OWNN = text.Replace(OWNA, "");
                                currentElement.OWNA = OWNA.Replace("\n", " ").Replace("\r ", " ").Replace(OWNC, "").Trim();
                                currentElement.OWNN = OWNN.Replace("\n", " ").Replace("of", "").Trim();
                                currentElement.OWNC = OWNC.Replace(".", "");
                            }
                            if (record.StartsWith(I740))
                            {
                                var text = record.Replace(I740, "").Trim();
                                var matchUppercase = Regex.Match(text, @"(\p{Lu}+\W{1,3}){2,}");
                                var matchPO = Regex.Match(text, @"P\. ?O\.");
                                var indexOfPO = -1;
                                if (matchPO.Success)
                                {
                                    indexOfPO = text.IndexOf(matchPO.Value);
                                }
                                var indexOfDot = text.IndexOf(".");
                                var CORN = "";
                                var CORA = "";
                                if(indexOfPO > -1)
                                {
                                    CORA = text.Substring(indexOfPO);
                                    CORN = text.Replace(CORA, "");
                                }
                                else if (matchUppercase.Success && !text.Contains("("))
                                {
                                    CORN = matchUppercase.Value;
                                    CORA = text.Replace(CORN, "");
                                }
                                else if(indexOfPO == -1 && indexOfDot > -1)
                                {
                                    CORA = text.Substring(indexOfDot + 1);
                                    CORN = text.Replace(CORA, "");
                                }
                                else
                                {
                                    var strings = text.Split('\n');
                                    text = text.Replace(strings[0], "").Trim();
                                    CORN = strings[0];
                                    CORA = text;
                                }
                                currentElement.CORA = CORA.Replace("\n", " ").Replace("\r", " ");
                                currentElement.CORN = CORN.Replace("\n", " ").Replace("\r", " ");
                            }
                            if (record.StartsWith(I511))
                            {
                                List<string> Classes = new List<string>();
                                List<string> Descs = new List<string>();
                                var text = record.Replace(I511, "").Trim();


                                var classValue = Regex.Match(text, @"C ?(l|H)ass ?(\d{1,2},? ?(and)? ?)+").Value;
                                var descValue = text.Replace(classValue, "");
                                var classes = Regex.Matches(classValue, @"\d{1,2}");
                                var descs = Regex.Matches(descValue, @"\p{Lu}\p{Ll}+\W{0,3}(\p{Ll}+\W{1,3})*");
                                for(int j = 0; j < descs.Count; j++)
                                {
                                    var end = descs[j].Length;
                                    var start = descValue.IndexOf(descs[j].Value);
                                    if(j + 1 < descs.Count)
                                    {
                                        end = descValue.IndexOf(descs[j + 1].Value);
                                        Descs.Add(descValue.Substring(start, end - start));
                                    }
                                    else
                                    {
                                        Descs.Add(descValue.Substring(descValue.IndexOf(descs[j].Value)));
                                    }

                                    if(classes.Count > j)
                                    {
                                        if (classes[j].Value.Length == 1)
                                            Classes.Add("0" + classes[j].Value);
                                        else
                                            Classes.Add(classes[j].Value);
                                    }
                                    else
                                    {
                                        if (classes[classes.Count - 1].Value.Length == 1)
                                            Classes.Add("0" + classes[classes.Count - 1].Value);
                                        else
                                            Classes.Add(classes[classes.Count - 1].Value);
                                    }
                                }

                                currentElement.CLAS = Classes;
                                currentElement.DESC = Descs;
                            }
                        }
                    }
                }
            }

            return elementsOut;
        }

        public static List<Ownerchange> OwnerChange(string input, Dictionary<string, string> Images)
        {
            List<Ownerchange> elementsOut = new List<Ownerchange>();

            if (!string.IsNullOrEmpty(input))
            {
                string[] splittedRecords = null;
                string tmpRecordValue;
                int tmpInc;
                var elements = input.Split('\n');
                for (int i = 0; i < elements.Length; i++)
                {
                    if (elements[i].StartsWith(I220))
                    {
                        var currentElement = new Ownerchange();
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            tmpRecordValue += elements[tmpInc] + "\n";
                            ++tmpInc;
                        } while (tmpInc < elements.Length && !elements[tmpInc].StartsWith(I220));

                        if (tmpRecordValue != null)
                            splittedRecords = Methods.RecString(tmpRecordValue);

                        foreach (var record in splittedRecords)
                        {
                            if (record.StartsWith(I210))
                            {
                                currentElement.APNR = record.Replace(I210, "").Replace("/", "_").Trim();
                            }
                            if (record.StartsWith(I220))
                            {
                                currentElement.APDA = Methods.DateNormalize(record.Replace(I220, "").Trim());
                            }
                            if (record.StartsWith(I731))
                            {
                                var text = record.Replace(I731, "").Trim();
                                var OWNC = Regex.Match(text, @"(\p{Lu}\p{Ll}+((-|\s)\p{L}+)?)$").Value;
                                if (string.IsNullOrEmpty(OWNC))
                                {
                                    OWNC = "U.S.A";
                                }
                                var index = text.IndexOf("of");
                                var OWNA = text.Substring(index + 2);
                                var OWNN = text.Replace(OWNA, "");
                                currentElement.OWNA = OWNA.Replace("\n", " ").Replace("\r ", " ").Replace(OWNC, "").Trim();
                                currentElement.OWNN = OWNN.Replace("\n", " ").Replace("of", "").Trim();
                                currentElement.OWNC = OWNC.Replace(".", "");
                            }
                        }
                    }
                }
            }

            return elementsOut;
        }

        public static List<Registrations> Registrations(string input, Dictionary<string, string> Images)
        {
            List<Registrations> elementsOut = new List<Registrations>();

            if (!string.IsNullOrEmpty(input))
            {
                string[] splittedRecords = null;
                string tmpRecordValue;
                int tmpInc;
                var elements = input.Split('\n');
                for (int i = 0; i < elements.Length; i++)
                {
                    if (elements[i].StartsWith(I151))
                    {
                        var currentElement = new Registrations();
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            tmpRecordValue += elements[tmpInc] + "\n";
                            ++tmpInc;
                        } while (tmpInc < elements.Length && !elements[tmpInc].StartsWith(I151));

                        if (tmpRecordValue != null)
                            splittedRecords = Methods.RecString(tmpRecordValue);

                        foreach (var record in splittedRecords)
                        {
                            if (record.StartsWith(I151))
                            {
                                currentElement.RGDA = Methods.DateNormalize(record.Replace(I151, "").Trim());
                                currentElement.TMTY = "M";
                            }
                            if (record.StartsWith(I111))
                            {
                                currentElement.APNR = record.Replace(I111, "").Replace("/", "_").Trim();
                                currentElement.RGNR = currentElement.APNR;
                            }
                            if (record.StartsWith(I540))
                            {
                                var image = record.Replace(I540, "").Trim();
                                if (!string.IsNullOrEmpty(image))
                                {
                                    currentElement.TMNM = image;
                                    currentElement.TMTY = "W";
                                }
                            }
                            if (record.StartsWith(I731))
                            {
                                var text = record.Replace(I731, "").Trim();
                                var OWNC = Regex.Match(text, @"(\p{Lu}\p{Ll}+((-|\s)\p{L}+)?)$").Value;
                                if (string.IsNullOrEmpty(OWNC))
                                {
                                    OWNC = "U.S.A";
                                }
                                var index = text.IndexOf("of");
                                var OWNA = text.Substring(index + 2);
                                var OWNN = text.Replace(OWNA, "");
                                currentElement.OWNA = OWNA.Replace("\n", " ").Replace("\r ", " ").Replace(OWNC, "").Trim();
                                currentElement.OWNN = OWNN.Replace("\n", " ").Replace("of", "").Trim();
                                currentElement.OWNC = OWNC.Replace(".", "");
                            }
                            if (record.StartsWith(I740))
                            {
                                var text = record.Replace(I740, "").Trim();
                                var matchUppercase = Regex.Match(text, @"(\p{Lu}+\W{1,3}){2,}");
                                var matchPO = Regex.Match(text, @"P\. ?O\.");
                                var indexOfPO = -1;
                                if (matchPO.Success)
                                {
                                    indexOfPO = text.IndexOf(matchPO.Value);
                                }
                                var indexOfDot = text.IndexOf(".");
                                var CORN = "";
                                var CORA = "";
                                if (indexOfPO > -1)
                                {
                                    CORA = text.Substring(indexOfPO);
                                    CORN = text.Replace(CORA, "");
                                }
                                else if (matchUppercase.Success && !text.Contains("("))
                                {
                                    CORN = matchUppercase.Value;
                                    CORA = text.Replace(CORN, "");
                                }
                                else if (indexOfPO == -1 && indexOfDot > -1)
                                {
                                    CORA = text.Substring(indexOfDot + 1);
                                    CORN = text.Replace(CORA, "");
                                }
                                else
                                {
                                    var strings = text.Split('\n');
                                    text = text.Replace(strings[0], "").Trim();
                                    CORN = strings[0];
                                    CORA = text;
                                }
                                currentElement.CORA = CORA.Replace("\n", " ").Replace("\r", " ");
                                currentElement.CORN = CORN.Replace("\n", " ").Replace("\r", " ");
                            }
                            if (record.StartsWith(I511))
                            {
                                List<string> Classes = new List<string>();
                                List<string> Descs = new List<string>();
                                var text = record.Replace(I511, "").Trim();
                                var classes = Regex.Matches(text, @"\d{1,2}");

                                foreach(Match item in classes)
                                {
                                    if (!string.IsNullOrEmpty(item.Value))
                                    {
                                        Classes.Add(item.Value);
                                        Descs.Add("S");
                                    }
                                }

                                currentElement.CLAS = Classes;
                                currentElement.DESC = Descs;
                            }
                            currentElement.NOTE = "DESC missing";
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

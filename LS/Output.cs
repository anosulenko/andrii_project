﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static LS.Elements;

namespace LS
{
    public class Output
    {
        public static void ApplicationToFile(List<Applications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_App.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        if (record.CORN != null && record.CORN != "")
                        {
                            sf.WriteLine("CORN:\t" + record.CORN);
                            sf.WriteLine("CORA:\t" + record.CORA);
                            sf.WriteLine("CORC:\t" + record.CORC);
                        }
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void OwnerchangeToFile(List<Ownerchange> output, DirectoryInfo pathprocessed)
        {
            var path = Path.Combine(pathprocessed.FullName, Directory.GetParent(pathprocessed.FullName).Name + "_Ownerchange.txt");
            var sf = new StreamWriter(path);
            if(output != null)
            {
                foreach(var record in output)
                {
                    sf.WriteLine("****");
                    sf.WriteLine("APNR:\t" + record.APNR);
                    sf.WriteLine("APDA:\t" + record.APDA);
                    sf.WriteLine("OWNN:\t" + record.OWNN);
                    sf.WriteLine("OWNA:\t" + record.OWNA);
                    sf.WriteLine("OWNC:\t" + record.OWNC);
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void RegistrationToFile(List<Registrations> output, DirectoryInfo pathprocessed)
        {
            var path = Path.Combine(pathprocessed.FullName, Directory.GetParent(pathprocessed.FullName).Name + "_Reg.txt");
            var sf = new StreamWriter(path);
            if(output != null)
            {
                foreach (var record in output)
                {
                    sf.WriteLine("****");
                    sf.WriteLine("APNR:\t" + record.APNR);
                    sf.WriteLine("RGNR:\t" + record.RGNR);
                    sf.WriteLine("RGDA:\t" + record.RGDA);
                    sf.WriteLine("TMNM:\t" + record.TMNM);
                    sf.WriteLine("TMTY:\t" + record.TMTY);
                    sf.WriteLine("OWNN:\t" + record.OWNN);
                    sf.WriteLine("OWNA:\t" + record.OWNA);
                    sf.WriteLine("OWNC:\t" + record.OWNC);
                    sf.WriteLine("CORN:\t" + record.CORN);
                    sf.WriteLine("CORA:\t" + record.CORA);
                    sf.WriteLine("CORC:\t" + record.CORC);
                    sf.WriteLine("NOTE:\t" + record.NOTE);
                    if (record.CLAS != null && record.CLAS.Count > 0)
                    {
                        for (int i = 0; i < record.CLAS.Count; i++)
                        {
                            sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                            sf.WriteLine("DESC:\t" + record.DESC[i]);
                        }
                    }
                }
            }
        }
    }
}

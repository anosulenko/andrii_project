﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KE_WF
{
    public class ProcessApplication
    {
        static readonly string I210 = "(210)"; //
        static readonly string I220 = "(220)"; //
        static readonly string I300 = "(300)"; //
        static readonly string I511 = "(511)"; //
        static readonly string I730 = "(730)"; //
        static readonly string I740 = "(740)"; //

        public static List<XElement> regList = null;
        public static FileInfo currentFile = null;

        public class DescClass
        {
            public string CLAS { get; set; }
            public string DESC { get; set; }
        }
        public class Priority
        {
            public string PRIC { get; set; }
            public string PRIN { get; set; }
            public string PRID { get; set; }
        }
        public class Owner
        {
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
        }
        public class Agent
        {
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
        }
        public class ElementsForOutput
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string PGNR { get; set; }
            public Priority PRIO { get; set; }
            public List<DescClass> Descs { get; set; }
            public Agent AGENT { get; set; }
            public Owner OWNER { get; set; }

        }

        static List<ElementsForOutput> ElementsOut = new List<ElementsForOutput>();
        public static void AppProcess()
        {
            var dir = new DirectoryInfo(KE.pdfDirectoryInfo);
            var outputFolderPath = KE.pdfDirectoryInfo.Replace("Input", "Output");
            var files = new List<string>();
            foreach (FileInfo file in dir.GetFiles("*.tetml", SearchOption.AllDirectories))
            {
                if (!Regex.IsMatch(file.Name, @"_Output.txt"))
                    files.Add(file.FullName);
            }
            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                string fileName = file;
                XElement tet = XElement.Load(fileName);
                regList = tet.Descendants().Where(d => d.Name.LocalName == "Text" || d.Name.LocalName == "Page").ToList();
                ElementsOut.Clear();
                Directory.CreateDirectory(outputFolderPath);
                string path = Path.Combine(outputFolderPath, Path.GetFileNameWithoutExtension(file) + "_Output.txt");
                StreamWriter sf = new StreamWriter(path);
                ElementsForOutput currentElement = null;

                string[] splittedRecord = null;
                string currentPageNumber = null;
                string tmpRecordValue;
                int tmpInc;
                for (int i = 0; i < regList.Count; ++i)
                {
                    if (regList[i].Name.LocalName == "Page")
                    {
                        currentPageNumber = regList[i].Attribute("number").Value;
                    }
                    var value = regList[i].Value;
                    if (value.StartsWith(I210))
                    {
                        currentElement = new ElementsForOutput();
                        //string lastImageId = null;
                        tmpRecordValue = "";
                        string tradeMarkName = "*";
                        tmpInc = i;
                        do
                        {
                            //if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            //{
                            //    lastImageId = elements[tmpInc].Attribute("image").Value;
                            //}
                            if (regList[tmpInc].Name.LocalName != "Page")
                            {
                                tmpRecordValue += regList[tmpInc].Value + "\n";
                            }
                            if (regList[tmpInc].Name.LocalName == "Text" && Convert.ToInt32(double.Parse(regList[tmpInc].Parent.Parent.Attribute("llx").Value.Replace(".", ","))) > 360)
                            {
                                tradeMarkName = regList[tmpInc].Value.Trim();
                            }
                            ++tmpInc;
                        } while (tmpInc < regList.Count() && !regList[tmpInc].Value.StartsWith(I210));
                        /*trademark name and image process*/
                        if (tradeMarkName != "*")
                        {
                            currentElement.TMNM = tradeMarkName;
                            currentElement.TMTY = "W";
                        }
                        else
                        {
                            currentElement.TMTY = "M";
                        }
                        /*end tmnm and image process*/

                        if (tmpRecordValue != null) splittedRecord = Methods.RecSplit(tmpRecordValue.Replace(tradeMarkName, ""));

                        if (tmpRecordValue.Contains("107869"))
                        {

                        }

                        foreach (var record in splittedRecord)
                        {
                            if (record.StartsWith(I210))
                            {
                                currentElement.APNR = record.Replace(":", "").Replace(I210, "").Replace("/", "_").Replace(" ", "").Trim();
                                currentElement.PGNR = currentPageNumber;
                            }
                            if (record.StartsWith(I220))
                            {
                                currentElement.APDA = Methods.DateNormalize(record.Replace(":", "").Replace(I220, "").Trim());
                            }
                            if (record.StartsWith(I300))
                            {
                                string tmpV = record.Replace(":", "").Replace(I300, "").Trim();
                                if (tmpV != "None" && tmpV != "Non None")
                                {
                                    currentElement.PRIO = Methods.PriorityProcess(record.Replace(":", "").Replace(I300, "").Trim());
                                }
                            }
                            if (record.StartsWith(I511))
                            {
                                currentElement.Descs = Methods.DescClassProcess(record.Replace(":", "").Replace(I511, "").Trim());
                            }
                            if (record.StartsWith(I730))
                            {
                                try
                                {
                                    currentElement.OWNER = Methods.OwnerProcess(record.Replace(":", "").Replace(I730, "").Trim());
                                }
                                catch (Exception)
                                {
                                    Console.WriteLine("Owner identification error in:\t" + currentElement.APNR);
                                }
                            }
                            if (record.StartsWith(I740))
                            {
                                string tmpAgent = record.Replace(":", "").Replace(I740, "").Trim();
                                try
                                {
                                    if (tmpAgent != "None")
                                    {
                                        currentElement.AGENT = Methods.AgentProcess(tmpAgent);
                                    }
                                }
                                catch (Exception e)
                                {

                                }
                            }
                        }
                        ElementsOut.Add(currentElement);
                    }
                }
                

                if (ElementsOut != null)
                {
                    foreach(var elemOut in ElementsOut)
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + elemOut.APNR);
                        sf.WriteLine("APDA:\t" + elemOut.APDA);
                        sf.WriteLine("TMNM:\t" + elemOut.TMNM);
                        sf.WriteLine("TMTY:\t" + elemOut.TMTY);
                        if (elemOut.PRIO != null)
                        {
                            sf.WriteLine("PRIC:\t" + elemOut.PRIO.PRIC);
                            sf.WriteLine("PRIN:\t" + elemOut.PRIO.PRIN);
                            sf.WriteLine("PRID:\t" + elemOut.PRIO.PRID);
                        }
                        if (elemOut.OWNER != null)
                        {
                            sf.WriteLine("OWNN:\t" + elemOut.OWNER.OWNN);
                            sf.WriteLine("OWNA:\t" + elemOut.OWNER.OWNA);
                            sf.WriteLine("OWNC:\t" + elemOut.OWNER.OWNC);
                        }
                        if (elemOut.AGENT != null)
                        {
                            sf.WriteLine("CORN:\t" + elemOut.AGENT.CORN);
                            sf.WriteLine("CORA:\t" + elemOut.AGENT.CORA);
                            sf.WriteLine("CORC:\t" + elemOut.AGENT.CORC);
                        }
                        if (elemOut.Descs != null && elemOut.Descs.Count > 0)
                        {
                            for (int i = 0; i < elemOut.Descs.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + elemOut.Descs[i].CLAS);
                                sf.WriteLine("DESC:\t" + elemOut.Descs[i].DESC);
                            }
                        }
                        sf.WriteLine("PGNR:\t" + elemOut.PGNR);
                    }
                }

                sf.Flush();
                sf.Close();
            }
        }
    }
}

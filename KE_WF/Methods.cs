﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace KE_WF
{
    public class Methods
    {
        public static string[] RecSplit(string recString)
        {
            string[] splittedRecord = null;
            string tempStrC = recString;

            tempStrC = tempStrC.Replace("<Text>", "").Replace("</Text>", "").Replace("<Box>", "").Replace("</Box>", "").Replace("<Para>", "").Replace("</Para>", "");
            tempStrC = Regex.Replace(tempStrC, "<Box.*?>", "");
            tempStrC = Regex.Replace(tempStrC, @"\s{2,}", " ");
            if (tempStrC != "")
            {
                if (tempStrC.Contains("("))
                {
                    Regex regexPatOne = new Regex(@"\(\d{3}\)", RegexOptions.IgnoreCase);
                    MatchCollection matchesClass = regexPatOne.Matches(tempStrC);
                    if (matchesClass.Count > 0)
                    {
                        foreach (Match matchC in matchesClass)
                        {
                            tempStrC = tempStrC.Replace(matchC.Value, "***" + matchC.Value);
                        }
                    }
                }
                /*Splitting record*/
                splittedRecord = tempStrC.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries);
            }
            return splittedRecord;
        }
        public static ProcessApplication.Priority PriorityProcess(string s)
        {
            ProcessApplication.Priority prio = new ProcessApplication.Priority();
            s = s.Replace(":", "").Trim();
            Regex prioPat = new Regex(@"No\.\s*(?<number>.*)\s*\bof\b\s*(?<date>\d{2}\/\d{2}\/\d{4})\s*in(\sthe)*\s*(?<country>.*)$");
            if (s != "None")
            {
                var a = prioPat.Match(s);
                prio.PRIC = CountryCodeReplacer(a.Groups["country"].Value);
                prio.PRIN = a.Groups["number"].Value.Trim();
                prio.PRID = DateNormalizePrio(a.Groups["date"].Value);
            }
            return prio;
        }

        public static ProcessApplication.Owner OwnerProcess(string s)
        {
            var ownerPat = new Regex(@"(?<name>^[^a-z]+)\s(?<address>.*)$"/*, RegexOptions.Singleline*/);
            s = s.Replace("\n", " ").Replace(":", "").Trim();
            var tmp = ownerPat.Match(s);
            if (tmp.Success)
            {
                var tmpOWNN = tmp.Groups["name"].Value.Trim();
                var tmpOWNA = tmp.Groups["address"].Value.Trim();
                if (tmpOWNA.StartsWith("of P.O. Box"))
                    tmpOWNA = tmpOWNA.Replace("of P.O. Box", "").Trim();
                return new ProcessApplication.Owner
                {
                    OWNN = tmpOWNN,
                    OWNA = tmpOWNA,
                    OWNC = CountryCodeReplacer(tmpOWNA)
                };
            }
            else return new ProcessApplication.Owner
            {
                OWNN = s,
                OWNA = "",
                OWNC = ""
            };
        }
        public static ProcessApplication.Agent AgentProcess(string s)
        {
            ProcessApplication.Agent agent = new ProcessApplication.Agent();
            s = s.Replace("\n", " ").Replace(":", "").Trim();
            var match = Regex.Match(s, @"P\.?O\.? Box").Value;
            if (!string.IsNullOrEmpty(match) && !s.Contains("None"))
            {
                var a = s.Split(new string[] { match }, StringSplitOptions.RemoveEmptyEntries).ToList();
                return new ProcessApplication.Agent
                {
                    CORN = a[0].Trim(),
                    CORA = a[1].Trim(),
                    CORC = "KE"
                };
            }
            return agent;
        }
        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            }
            return dateNormalized;
        }
        public static string DateNormalizePrio(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + "-" + date.Groups["month"].Value + "-" + date.Groups["day"].Value;
            }
            return dateNormalized;
        }
        //public static void ExportApplications(List<ProcessApplication.ElementsForOutput> output, DirectoryInfo pathProcessed)
        //{
        //    var path = Path.Combine(pathProcessed.FullName, Path.GetFileNameWithoutExtension(KE_WF.currentFile.FullName) + "_App.txt");
        //    var sf = new StreamWriter(path);
        //    if (output != null)
        //    {
        //        foreach (var record in output)
        //        {
        //            try
        //            {
        //                sf.WriteLine("****");
        //                sf.WriteLine("APNR:\t" + record.APNR);
        //                sf.WriteLine("APDA:\t" + record.APDA);
        //                sf.WriteLine("TMNM:\t" + record.TMNM);
        //                sf.WriteLine("TMTY:\t" + record.TMTY);
        //                if (record.PRIO.PRIN != null)
        //                {
        //                    sf.WriteLine("PRIC:\t" + record.PRIO.PRIC);
        //                    sf.WriteLine("PRIN:\t" + record.PRIO.PRIN);
        //                    sf.WriteLine("PRID:\t" + record.PRIO.PRID);
        //                }
        //                if (record.OWNER.OWNN != null)
        //                {
        //                    sf.WriteLine("OWNN:\t" + record.OWNER.OWNN);
        //                    sf.WriteLine("OWNA:\t" + record.OWNER.OWNA);
        //                    sf.WriteLine("OWNC:\t" + record.OWNER.OWNC);
        //                }
        //                if (record.AGENT.CORN != null)
        //                {
        //                    sf.WriteLine("CORN:\t" + record.AGENT.CORN);
        //                    sf.WriteLine("CORA:\t" + record.AGENT.CORA);
        //                    sf.WriteLine("CORC:\t" + record.AGENT.CORC);
        //                }
        //                if (record.Descs.Count > 0)
        //                {
        //                    for (int i = 0; i < record.Descs.Count; i++)
        //                    {
        //                        sf.WriteLine("CLAS:\t" + record.Descs[i].CLAS);
        //                        sf.WriteLine("DESC:\t" + record.Descs[i].DESC);
        //                    }
        //                }
        //                sf.WriteLine("PGNR:\t" + record.PGNR);
        //            }
        //            catch (Exception)
        //            {
        //                Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
        //            }
        //        }
        //    }
        //    sf.Flush();
        //    sf.Close();
        //}

        public static string ClassNormalize(string s)
        {
            if (s.Trim().Length == 1) return 0 + s;
            else return s;
        }

        internal static List<ProcessApplication.DescClass> DescClassProcess(string v)
        {
            List<ProcessApplication.DescClass> list = new List<ProcessApplication.DescClass>();
            v = v.Replace("\n", " ").Replace(":", "").Trim();
            Regex pat = new Regex(@"(?=\b\d+\()", RegexOptions.Singleline);
            Regex patSpl = new Regex(@"(?<class>^\d+)\s*\(\s*(?<desc>.*)\)*$");
            var a = pat.Split(v).Where(x => x != "").ToList();
            foreach (var rec in a)
            {
                var b = patSpl.Match(rec.Trim());
                list.Add(new ProcessApplication.DescClass
                {
                    CLAS = ClassNormalize(b.Groups["class"].Value),
                    DESC = b.Groups["desc"].Value.Trim('.').Trim(',').Trim(')')
                });
            }
            return list;
        }

        public static string CountryCodeReplacer(string ccStr)
        {
            List<string> ccFullNames = new List<string> { "england", ", uae", "scotland", "tanzania", "hong kong", "republic of korea", "afghanistan", "aland islands", "albania", "algeria", "american samoa", "andorra", "angola", "anguilla", "antarctica", "antigua and barbuda", "argentina", "armenia", "aruba", "australia", "austria", "azerbaijan", "bahamas", "bahrain", "bangladesh", "barbados", "belarus", "belgium", "belize", "benin", "bermuda", "bhutan", "bolivia", "bosnia and herzegovina", "botswana", "bouvet island", "brazil", "british virgin islands", "british indian ocean territory", "brunei darussalam", "bulgaria", "burkina faso", "burundi", "cambodia", "cameroon", "canada", "cape verde", "cayman islands", "central african republic", "chad", "chile", "china", "hong kong, sar china", "macao, sar china", "christmas island", "cocos (keeling) islands", "colombia", "comoros", "congo (brazzaville)", "congo, (kinshasa)", "cook islands", "costa rica", "côte d'ivoire", "croatia", "cuba", "cyprus", "czech republic", "denmark", "djibouti", "dominica", "dominican republic", "ecuador", "egypt", "el salvador", "equatorial guinea", "eritrea", "estonia", "ethiopia", "falkland islands (malvinas)", "faroe islands", "fiji", "finland", "france", "french guiana", "french polynesia", "french southern territories", "gabon", "gambia", "georgia", "germany", "ghana", "gibraltar", "greece", "greenland", "grenada", "guadeloupe", "guam", "guatemala", "guernsey", "guinea", "guinea-bissau", "guyana", "haiti", "heard and mcdonald islands", "holy see (vatican city state)", "honduras", "hungary", "iceland", "india", "indonesia", "iran, islamic republic of", "iraq", "ireland", "isle of man", "israel", "italy", "jamaica", "japan", "jersey", "jordan", "kazakhstan", "kenya", "kiribati", "korea (north)", "korea (south)", "kuwait", "kyrgyzstan", "lao pdr", "latvia", "lebanon", "lesotho", "liberia", "libya", "liechtenstein", "lithuania", "luxembourg", "macedonia, republic of", "madagascar", "malawi", "malaysia", "maldives", "mali", "malta", "marshall islands", "martinique", "mauritania", "mauritius", "mayotte", "mexico", "micronesia, federated states of", "moldova", "monaco", "mongolia", "montenegro", "montserrat", "morocco", "mozambique", "myanmar", "namibia", "nauru", "nepal", "netherlands", "netherlands antilles", "new caledonia", "new zealand", "nicaragua", "niger", "nigeria", "niue", "norfolk island", "northern mariana islands", "norway", "oman", "pakistan", "palau", "palestinian territory", "panama", "papua new guinea", "paraguay", "peru", "philippines", "pitcairn", "poland", "portugal", "puerto rico", "qatar", "réunion", "romania", "russian federation", "rwanda", "saint-barthélemy", "saint helena", "saint kitts and nevis", "saint lucia", "saint-martin (french part)", "saint pierre and miquelon", "saint vincent and grenadines", "samoa", "san marino", "sao tome and principe", "saudi arabia", "senegal", "serbia", "seychelles", "sierra leone", "singapore", "slovakia", "slovenia", "solomon islands", "somalia", "south africa", "south georgia and the south sandwich islands", "south sudan", "spain", "sri lanka", "sudan", "suriname", "svalbard and jan mayen islands", "swaziland", "sweden", "switzerland", "syrian arab republic (syria)", "taiwan, republic of china", "tajikistan", "tanzania, united republic of", "thailand", "timor-leste", "togo", "tokelau", "tonga", "trinidad and tobago", "tunisia", "turkey", "turkmenistan", "turks and caicos islands", "tuvalu", "uganda", "ukraine", "united arab emirates", "united kingdom", "united states of america", "us minor outlying islands", "uruguay", "uzbekistan", "vanuatu", "venezuela (bolivarian republic)", "viet nam", "virgin islands, us", "wallis and futuna islands", "western sahara", "yemen", "zambia", "zimbabwe", "united states", ", usa" };
            List<string> ccShortNames = new List<string> { "GB", "AE", "GB", "TZ", "HK", "KR", "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "VG", "IO", "BN", "BG", "BF", "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "HK", "MO", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI", "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI", "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW", "GY", "HT", "HM", "VA", "HN", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE", "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MK", "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "ME", "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS", "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "ST", "SA", "SN", "RS", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "SS", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VI", "WF", "EH", "YE", "ZM", "ZW", "US", "US" };
            foreach (var country in ccFullNames)
            {
                if (ccStr.ToLower().Contains(country))
                {
                    return ccShortNames.ElementAt(ccFullNames.IndexOf(country));
                }
            }
            return ccStr;
        }
    }
}

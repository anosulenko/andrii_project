﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AU
{
    public class Elements
    {
        public class SubCode4
        {
            public string AppNumber { get; set; }
            public string EventDate { get; set; }
        }

        public class SubCode5
        {
            public string AppNumber { get; set; }
            public string EventDate { get; set; }
        }

        public class SubCode6
        {
            public string AppNumber { get; set; }
            public string EventDate { get; set; }
        }
    }
}

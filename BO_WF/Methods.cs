﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BO_WF
{
    public class Methods
    {
        public struct ApplicationNumberAndDate
        {
            public string Number { get; set; }
            public string Date { get; set; }
        }
        internal static ApplicationNumberAndDate NumberAndDate(string v)
        {
            Match k = null;
            Regex pat = new Regex(@"(?<number>.*)\s*(?<date>\d{2}[-/\.]\d{2}[-/\.]\d{4})");
            if (pat.Match(v).Success) k = pat.Match(v);
            ApplicationNumberAndDate application = new ApplicationNumberAndDate
            {
                Number = k.Groups["number"].Value.Replace(" ", "").Trim(),
                Date = DateNormalize(k.Groups["date"].Value.Trim())
            };
            return application;
        }

        public static string ClassZeroAdd(string value)
        {
            if (value.Length == 1) return "0" + value;
            else return value;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            }
            return dateNormalized;
        }
        public static string DateNormalizePrid(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + "-" + date.Groups["month"].Value + "-" + date.Groups["day"].Value;
            }
            return dateNormalized;
        }

        public static string RecordSplit(string s)
        {
            s = s.Replace("<Text>", "").Replace("</Text>", "").Replace("<Box>", "").Replace("</Box>", "").Replace("<Para>", "").Replace("</Para>", "");
            s = Regex.Replace(s, "<Box.*?>", "");
            s = Regex.Replace(s, @"\s{2,}", " ");

            return s;
        }


        public static string[] RecSplit(string recString)
        {
            string[] splittedRecord = null;
            string tempStrC = recString
                .Replace("NÚMERO DE PUBLICACIÓN", "((200))") //Beginning
                .Replace("NÚMERO DE SOLICITUD", "((210))") //APNR
                .Replace("220 Tanggal Penerimaan", "((220))") //APDA
                .Replace("NÚMERO DE PRIORIDAD", "((310))") //PRIN
                .Replace("FECHA DE PRIORIDAD", "((320))") //PRID
                //.Replace("NÚMERO DE PRIORIDAD", "((330))") //PRIC
                .Replace("TIPO DE SIGNO", "") //skip
                .Replace("GENERO DEL SIGNO", "") //skip
                .Replace("FECHA DE SOLICITUD", "") //skip
                .Replace("NOMBRE DEL TITULAR", "((730))") //Owner name
                .Replace("DIRECCIÓN DEL TITULAR", "((731))") //Owner address
                .Replace("PAÍS DEL TITULAR", "((732))") //Owner country
                .Replace("NOMBRE DEL SIGNO", "((541))") //TMNM
                .Replace("NOMBRE DEL APODERADO", "((740))") //Agent name
                .Replace("DIRECCIÓN DEL APODERADO", "((741))") //Agent address
                .Replace("CLASE INTERNACIONAL", "((510))") //Class
                .Replace("PRODUCTOS", "((511))") //Desc
                .Replace("DESCRIPCIÓN", "((999))") //skip
                .Replace("Marca Producto", "") //skip
                .Replace("Marca Servicio", "") //skip
                .Replace("Denominación", "") //skip
                .Replace("Mixta", "") //skip
                .Replace("Figurativa", "") //skip
                .Replace("\n", " ") //TMNM
                .Trim();
            if (tempStrC != "")
            {
                if (tempStrC.Contains("("))
                {
                    Regex regexPatOne = new Regex(@"\(\(\d{3}\)\)", RegexOptions.IgnoreCase);
                    MatchCollection matchesClass = regexPatOne.Matches(tempStrC);
                    if (matchesClass.Count > 0)
                    {
                        foreach (Match matchC in matchesClass)
                        {
                            tempStrC = tempStrC.Replace(matchC.Value, "***" + matchC.Value);
                        }
                    }
                }
                /*Splitting record*/
                splittedRecord = tempStrC.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries);
            }
            return splittedRecord/*.Select(x => x.Replace("-\n", "").Replace("\n"," ").Trim()).ToArray()*/;
        }
    }
}

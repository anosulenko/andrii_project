﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace BO_WF
{
    public class ProcessApplications
    {
        static readonly string I200Beginning = "NÚMERO DE PUBLICACIÓN";
        static readonly string I200 = "((200))";
        static readonly string I210 = "((210))"; //APNR
        static readonly string I220 = "((220))"; //APDA
        static readonly string I310 = "((310))"; //PRIN
        static readonly string I320 = "((320))"; //PRID
        static readonly string I510 = "((510))"; //CLAS
        static readonly string I511 = "((511))"; //DESC
        static readonly string I541 = "((541))"; //Trademark Name
        static readonly string I730 = "((730))"; //Owner Name
        static readonly string I731 = "((731))"; //Owner Address
        static readonly string I732 = "((732))"; //Owner Country
        static readonly string I740 = "((740))"; //Agent name
        static readonly string I741 = "((741))"; //Agent address


        class ElementsForOutput
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string PRIC { get; set; }
            public string PRIN { get; set; }
            public string PRID { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string DESC { get; set; }
            public string CLAS { get; set; }
        }

        static List<ElementsForOutput> ElementsOut = new List<ElementsForOutput>();

        public static DirectoryInfo processed;
        public static void AppProcess()
        {
            var dir = new DirectoryInfo(BO.pdfDirectoryInfo);
            var outputFolderPath = BO.pdfDirectoryInfo.Replace("Input", "Output");
            var files = new List<string>();
            foreach(FileInfo file in dir.GetFiles("*.tetml", SearchOption.AllDirectories))
            {
                if (!Regex.IsMatch(file.Name, @"_Output.tetml"))
                    files.Add(file.FullName);
            }
            foreach(var txtFile in files)
            {
                ElementsOut.Clear();
                Directory.CreateDirectory(outputFolderPath);
                string path = Path.Combine(outputFolderPath, Path.GetFileNameWithoutExtension(txtFile) + "_Output.txt");
                StreamWriter sf = new StreamWriter(path);
                ElementsForOutput currentElement = null;
                var tst = File.ReadAllText(txtFile).Replace(I200Beginning, "***" + I200Beginning);
                var splittedRecords = Regex.Split(tst, @"\*\*\*");
                foreach (var record in splittedRecords)
                {
                    if (record.StartsWith(I200Beginning))
                    {
                        var newRecord = Methods.RecordSplit(record);
                        if (!string.IsNullOrEmpty(newRecord))
                        {
                            if (newRecord.StartsWith(I200Beginning))
                            {
                                currentElement = new ElementsForOutput();
                                ElementsOut.Add(currentElement);
                                bool emptyAgent = false;
                                string tmpRecValue = "";
                                string[] splittedRev = null;

                                if (newRecord.Contains("UNIPERSONAL")) emptyAgent = true;

                                splittedRev = Methods.RecSplit(newRecord);

                                foreach (var recordValue in splittedRev)
                                {
                                    if (recordValue.StartsWith(I210))
                                    {
                                        var tmp = Methods.NumberAndDate(recordValue.Replace(I210, "").Trim());
                                        currentElement.APNR = tmp.Number;
                                        currentElement.APDA = tmp.Date;
                                    }
                                    if (recordValue.StartsWith(I510))
                                    {
                                        currentElement.CLAS = Methods.ClassZeroAdd(recordValue.Replace(I510, "").Trim());
                                    }
                                    if (recordValue.StartsWith(I511))
                                    {
                                        var index = recordValue.IndexOf("</Content>");
                                        var desc = recordValue;
                                        if (index > 0)
                                        {
                                            desc = recordValue.Substring(0, recordValue.IndexOf("</Content>"));
                                        }
                                        currentElement.DESC = desc.Replace(I511, "").Trim();
                                    }
                                    if (recordValue.StartsWith(I541))
                                    {
                                        currentElement.TMNM = recordValue.Replace(I541, "").Trim();
                                        if (!string.IsNullOrEmpty(currentElement.TMNM)) currentElement.TMTY = "W";
                                        else currentElement.TMTY = "M";

                                    }
                                    if (recordValue.StartsWith(I310))
                                    {
                                        string tmp = recordValue.Replace(I310, "").Trim();
                                        var k = Regex.Match(tmp, @"(?<number>.*)PAÍS\s*(?<country>[A-Z]{2})");
                                        if (k.Success)
                                        {
                                            currentElement.PRIN = k.Groups["number"].Value.Trim();
                                            currentElement.PRIC = k.Groups["country"].Value.Trim();
                                        }
                                        else
                                        {
                                            Console.WriteLine("Priority identification error:\t" + currentElement.APNR);
                                        }
                                    }
                                    if (recordValue.StartsWith(I320))
                                    {
                                        currentElement.PRID = Methods.DateNormalizePrid(recordValue);
                                    }
                                    if (recordValue.StartsWith(I730))
                                    {
                                        currentElement.OWNN = recordValue.Replace(I730, "").Trim();
                                    }
                                    if (recordValue.StartsWith(I731))
                                    {
                                        currentElement.OWNA = recordValue.Replace(I731, "").Trim(',').Trim();
                                    }
                                    if (recordValue.StartsWith(I732))
                                    {
                                        string tmpValue = recordValue.Replace(I732, "").Trim();
                                        if (Regex.Match(tmpValue, @"(?<country>[A-Z]{2})\s+\w+.*").Success)
                                            currentElement.OWNC = Regex.Match(tmpValue, @"(?<country>[A-Z]{2})\s+\w+.*").Groups["country"].Value.Trim();
                                        else
                                            currentElement.OWNC = tmpValue;
                                    }
                                    if (recordValue.StartsWith(I740) && !emptyAgent)
                                    {
                                        currentElement.CORN = recordValue.Replace(I740, "").Trim();
                                    }
                                    if (recordValue.StartsWith(I741) && !emptyAgent)
                                    {
                                        currentElement.CORA = recordValue.Replace(I741, "").Trim(',').Trim();
                                        currentElement.CORC = "BO";
                                    }
                                }
                            }
                        }
                    }
                }

                if(ElementsOut != null)
                {
                    foreach(var elemOut in ElementsOut)
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t{0}", elemOut.APNR);
                        sf.WriteLine("APDA:\t{0}", elemOut.APDA);
                        sf.WriteLine("TMNM:\t{0}", elemOut.TMNM);
                        sf.WriteLine("TMTY:\t{0}", elemOut.TMTY);
                        if (elemOut.OWNN != null) sf.WriteLine("OWNN:\t{0}", elemOut.OWNN);
                        if (elemOut.OWNA != null) sf.WriteLine("OWNA:\t{0}", elemOut.OWNA);
                        if (elemOut.OWNC != null) sf.WriteLine("OWNC:\t{0}", elemOut.OWNC);
                        if (elemOut.CORN != null) sf.WriteLine("CORN:\t{0}", elemOut.CORN);
                        if (elemOut.CORA != null) sf.WriteLine("CORA:\t{0}", elemOut.CORA);
                        if (elemOut.CORC != null) sf.WriteLine("CORC:\t{0}", elemOut.CORC);
                        if (elemOut.PRIC != null) sf.WriteLine("PRIC:\t{0}", elemOut.PRIC);
                        if (elemOut.PRIN != null) sf.WriteLine("PRIN:\t{0}", elemOut.PRIN);
                        if (elemOut.PRID != null) sf.WriteLine("PRID:\t{0}", elemOut.PRID);
                        if (elemOut.CLAS != null) sf.WriteLine("CLAS:\t{0}", elemOut.CLAS);
                        if (elemOut.DESC != null) sf.WriteLine("DESC:\t{0}", elemOut.DESC);
                    }
                }

                sf.Flush();
                sf.Close();
            }
        }
    }
}
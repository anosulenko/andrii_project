﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BO_WF
{
    public partial class BO : Form
    {
        public static string pdfDirectoryInfo;
        public static bool OppositionDateFormat { get; set; } = false;
        string pdfFileName;
        string dirForPDF, inputDirGet, inputDirSet, outputDirSet;
        public BO()
        {
            InitializeComponent();
        }

        private void ChoosePDF_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "PDF files | *.pdf";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                pdfFileName = openFileDialog.SafeFileName;
                try
                {
                    inputDirGet = Directory.GetParent(Directory.GetCurrentDirectory()).FullName;
                    inputDirSet = Path.Combine(inputDirGet, "Input");
                    outputDirSet = Path.Combine(inputDirGet, "Output");
                    if (!Directory.Exists(inputDirSet))
                    {
                        Directory.CreateDirectory(inputDirSet);
                    }
                    if (!Directory.Exists(outputDirSet))
                    {
                        Directory.CreateDirectory(outputDirSet);
                    }
                    dirForPDF = Path.Combine(inputDirSet, pdfFileName.Remove(pdfFileName.IndexOf(".")));
                    Directory.CreateDirectory(dirForPDF);
                }
                catch (Exception)
                {
                    MessageBox.Show("Folder named " + pdfFileName + " already exist.");
                }
                try
                {
                    File.Copy(openFileDialog.FileName, Path.Combine(dirForPDF, pdfFileName));
                }
                catch (Exception)
                {
                    MessageBox.Show("File " + pdfFileName + "in " + dirForPDF + " already exist.");
                }
            }
        }

        private void RunTetTool_Click(object sender, EventArgs e)
        {
            DirectoryInfo pdfDir = null;
            if (dirForPDF != null)
            {
                pdfDir = new DirectoryInfo(dirForPDF);
            }
            MessageBox.Show("Please wait for the process success message");
            try
            {
                string cmdChoosenValue = null;
                cmdChoosenValue = File.ReadAllText(@".\ProcessApp.txt");

                ProcessStartInfo cmdInfoCommands = new ProcessStartInfo();
                cmdInfoCommands.FileName = "cmd.exe";
                cmdInfoCommands.WorkingDirectory = Directory.GetCurrentDirectory();
                cmdInfoCommands.Arguments = "/c " + cmdChoosenValue + dirForPDF + " " + dirForPDF + @"\*.pdf";
                cmdInfoCommands.WindowStyle = ProcessWindowStyle.Hidden;

                using (Process exeProcess = Process.Start(cmdInfoCommands))
                {
                    exeProcess.WaitForExit();
                    MessageBox.Show("TETml file extracted");
                }

            }
            catch (Exception)
            {
                MessageBox.Show("CMD error");
            }
        }

        private void ProccessTET_Click(object sender, EventArgs e)
        {
            try
            {
                pdfDirectoryInfo = dirForPDF;
                ProcessApplications.AppProcess();
                MessageBox.Show("Done! Check files in Output folder");
            }
            catch(Exception)
            {
                MessageBox.Show("TETml processing error occured");
            }
        }
    }
}

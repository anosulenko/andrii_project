﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static CW.Elements;

namespace CW
{
    public class ProcessingRenewals
    {
        public static string I01 = "01 Registration";
        public static string I01nl = "01 Inschrijvingsnummer";
        public static string I02 = "02 Expiry date";
        public static string I02nl = "02 Vervaldatum";
        public static string I03 = "03 Name";
        public static string I03nl = "03 Naam";
        public static string I04 = "04 Address";
        public static string I04nl = "04 Adres";
        public static string I05 = "05 Postal";
        public static string I05nl = "05 Postcode";
        public static string I06 = "06 Name";
        public static string I06nl = "06 Naam";
        public static string I08 = "08 Reproduction";
        public static string I08nl = "08 Afbeelding";
        public static string I09 = "09 Reproduction";
        public static string I09nl = "09 Afbeelding";
        public static string I12 = "12 In";
        public static string I13 = "13 Classification";
        public static string I13nl = "13 Klasse-aanduiding";
        public static string I14 = "14 Enumeration";
        public static string I14nl = "14 Klasse-opsomming";
        public static string I15 = "15 Date";
        public static string I15nl = "15 Datum";

        public static DirectoryInfo processed;

        public static List<Renewals> Renewals(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Renewals> elementsOut = new List<Renewals>();

            if (elements != null && elements.Count > 0)
            {
                List<string> splittedRecord = new List<string>();
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].Name.LocalName == "Page")
                    {
                        pgnr = elements[i].Attribute("number").Value;
                    }
                    var value = elements[i].Value;
                    if (value.StartsWith(I01) || value.StartsWith(I01nl))
                    {
                        var currentElement = new Renewals();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Replace("_", "").Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.StartsWith(I01) && !elements[tmpInc].Value.StartsWith(I01nl));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue, new string[] { I01, I01nl, I02, I02nl, I03, I03nl, I04, I04nl, I05, I05nl, I06, I06nl, I08, I08nl, I09, I09nl, I12, I13, I13nl, I14, I14nl, I15, I15nl });
                        }

                        foreach (var record in splittedRecord)
                        {
                            if (record.StartsWith("08"))
                            {
                                var text = record.Replace("\n", " ")
                                    .Replace("08 Reproduction of word mark", "")
                                    .Replace("08 Afbeelding van het woordmerk", "")
                                    .Trim();
                                currentElement.TMNM = text;
                                if (!string.IsNullOrEmpty(currentElement.TMNM))
                                {
                                    currentElement.TMTY = "W";
                                }
                            }
                            if (record.StartsWith("09"))
                            {
                                currentElement.TMTY = "M";
                            }
                            if (record.StartsWith("01"))
                            {
                                var text = record.Replace("\n", " ");
                                var pattern = new Regex(@"(?<RGNR>(Registration number|Inschrijvingsnummer)\s*\d+)\s*(?<APNR>(Nummer en dagtekening|Number and date)[^0-9]+ VD-[0-9]+)\s*(?<RWDA>\d{1,2}[^0-9]+[0-9]+)");
                                var match = pattern.Match(text);
                                if (match.Success)
                                {
                                    currentElement.RGNR = match.Groups["RGNR"].Value
                                        .Replace("Registration number", "")
                                        .Replace("Inschrijvingsnummer", "")
                                        .Trim();
                                    currentElement.APNR = match.Groups["APNR"].Value
                                        .Replace("Number and date of receipt (day and hour) of the filing", "")
                                        .Replace("Nummer en dagtekening (dag en uur) van het depot", "")
                                        .Replace("-", "")
                                        .Trim();
                                    currentElement.RWDA = Methods.ToRWDA(match.Groups["RWDA"].Value);
                                }
                                processed = Directory.CreateDirectory(Path.Combine(CW_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(CW_main.currentFile.FullName) + "\\Renewals"));
                                if (lastImageId != null)
                                {
                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(CW_main.currentFile.DirectoryName, Images[lastImageId]);
                                    if (File.Exists(fileName))
                                        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR.Replace("VD", "")) + ext); }
                                        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                }
                            }
                            if (record.StartsWith("02"))
                            {
                                var text = record.Replace("02 Expiry date", "")
                                    .Replace("Vervaldatum", "").Trim();
                                var match = Regex.Match(text, @"[^0-9]+");
                                var month = Methods.ToDate(match.Value);
                                var split = text.Split(match.Value);
                                if (split[0].Length == 1)
                                {
                                    text = "0" + text;
                                }
                                text = text.Replace(match.Value, month);
                                text = Methods.DateNormalize(text);
                                currentElement.EXDA = text;
                            }
                            if (record.StartsWith("03"))
                            {
                                currentElement.OWNN = record.Replace("\n", " ")
                                    .Replace("03 Name of the depositor", "")
                                    .Replace("03 Naam van de deposant", "").Trim();
                            }
                            if (record.StartsWith("04") || record.StartsWith("05"))
                            {
                                currentElement.OWNA = record.Replace("\n", " ").Replace("04 Address (street and number) of the depositor", "")
                                    .Replace("05 Postal code and place of the depositor", "")
                                    .Replace("04 Adres (straat en nummer) van de deposant", "")
                                    .Replace("05 Postcode, plaats en land van de deposant", "").Trim();
                                currentElement.OWNC = Methods.ToOWNC(currentElement.OWNA);
                            }
                            if (record.StartsWith("06"))
                            {
                                var text = record.Replace("06 Name and address of the trademark agent or", "")
                                    .Replace("address for correspondence of the depositor", "")
                                    .Replace("06 Naam en adres van de gemachtigde of", "")
                                    .Replace("vermelding van het correspondentie-adres van", "")
                                    .Replace("de deposant", "").Trim();
                                var split = text.Split('\n');
                                currentElement.CORN = split[0].Trim();
                                text = text.Replace(currentElement.CORN, "").Trim();
                                currentElement.CORA = text.Replace("\n", " ");
                                currentElement.CORC = Methods.ToOWNC(text);
                            }
                            if (record.StartsWith("13"))
                            {
                                List<string> classes = new List<string>();
                                List<string> descs = new List<string>();

                                var text = record
                                    .Replace("\n", " ")
                                    .Replace("13 Classification and specification of goods and services", "")
                                    .Replace("13 Klasse-aanduiding en opgave van de waren en diensten", "").Trim();
                                var index = text.IndexOf("14");
                                if (index != -1)
                                {
                                    text = text.Substring(0, index);
                                }
                                var pattern = new Regex(@"(?<Class>(Class|Klasse)\s*\d{2}):(?<DESC>.+?\.\n)", RegexOptions.Singleline);

                                var matches = pattern.Matches(record);
                                foreach (Match match in matches)
                                {
                                    classes.Add(match.Groups["Class"].Value
                                        .Replace("Class", "")
                                        .Replace("Klasse", "").Trim());
                                    descs.Add(match.Groups["DESC"].Value.Replace("\n", " ").Trim(' ', ':', ' '));
                                }
                                currentElement.DESC = descs;
                                currentElement.CLAS = classes;
                            }
                            if (record.StartsWith("15"))
                            {
                                var text = record.Replace("\n", " ").Replace("15 Date of depot", "").Trim();
                                var match = Regex.Match(text, "[^0-9]+");
                                var month = Methods.ToDate(match.Value);
                                var split = text.Split(' ');
                                if (split[0].Length == 1)
                                    text = $"0{text}";
                                currentElement.RGDA = Methods.DateNormalize(text.Replace(match.Value, month));
                            }
                            currentElement.PGNR = pgnr;
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

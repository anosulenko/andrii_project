﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace CW
{
    class CW_main
    {
        public static DirectoryInfo PathToTetml = new DirectoryInfo(@"C:\Users\Razrab\Desktop\CW\");
        public static FileInfo currentFile = null;
        static void Main(string[] args)
        {
            var files = new List<string>();
            foreach (FileInfo file in PathToTetml.GetFiles("*.tetml", SearchOption.AllDirectories))
                files.Add(file.FullName);

            

            foreach (var file in files)
            {
                XElement elem = null;
                List<XElement> newList = new List<XElement>();
                List<XElement> newImageList = new List<XElement>();
                List<XElement> regImageList = new List<XElement>();
                List<XElement> regList = new List<XElement>();
                Dictionary<string, string> regImages;
                Dictionary<string, string> newImages;
                regImages = new Dictionary<string, string>();
                newImages = new Dictionary<string, string>();
                currentFile = new FileInfo(file);
                elem = XElement.Load(file);

                if (currentFile.Name.EndsWith("Registrations.tetml"))
                {
                    regList = elem.Descendants().Where(e => e.Name.LocalName == "Text" || e.Name.LocalName == "PlacedImage" || e.Name.LocalName == "Page")
                        .ToList();
                    regImageList = elem.Descendants().Where(i => i.Name.LocalName == "Image")
                        .ToList();
                }

                if (currentFile.Name.EndsWith("Renewals.tetml"))
                {
                    newList = elem.Descendants().Where(e => e.Name.LocalName == "Text" || e.Name.LocalName == "PlacedImage" || e.Name.LocalName == "Page")
                        .ToList();
                    newImageList = elem.Descendants().Where(i => i.Name.LocalName == "Image")
                        .ToList();
                }
                
                

                foreach (var image in regImageList)
                {
                    var value = image.Attribute("id")?.Value;
                    if (value != null)
                    {
                        regImages.Add(value, image.Attribute("filename")?.Value);
                    }
                }

                foreach (var image in newImageList)
                {
                    var value = image.Attribute("id")?.Value;
                    if (value != null)
                    {
                        newImages.Add(value, image.Attribute("filename")?.Value);
                    }
                }

                if (regList.Count > 0)
                {
                    var processedRecords = ProcessingRegistrations.Registrations(regList, regImages);
                    Output.RegistrationsToFile(processedRecords, ProcessingRegistrations.processed);
                }
                if(newList.Count > 0)
                {
                    var processedRecords = ProcessingRenewals.Renewals(newList, newImages);
                    Output.RenewalsToFile(processedRecords, ProcessingRenewals.processed);
                }
            }
        }
    }
}

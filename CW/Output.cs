﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static CW.Elements;

namespace CW
{
    public class Output
    {
        public static void RegistrationsToFile(List<Registrations> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Reg.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("RGNR:\t" + record.RGNR);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        if (record.CORN != null)
                        {
                            sf.WriteLine("CORN:\t" + record.CORN);
                            sf.WriteLine("CORA:\t" + record.CORA);
                            sf.WriteLine("CORC:\t" + record.CORC);
                        }
                        for (int i = 0; i < record.DESC.Count; i++)
                        {
                            sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                            sf.WriteLine("DESC:\t" + record.DESC[i]);
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void RenewalsToFile(List<Renewals> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Ren.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("RGNR:\t" + record.RGNR);
                        sf.WriteLine("RGDA:\t" + record.RGDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("RWDA:\t" + record.RWDA);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        if (record.CORN != null)
                        {
                            sf.WriteLine("CORN:\t" + record.CORN);
                            sf.WriteLine("CORA:\t" + record.CORA);
                            sf.WriteLine("CORC:\t" + record.CORC);
                        }
                        if(record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }
                        
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

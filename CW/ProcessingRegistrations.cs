﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static CW.Elements;

namespace CW
{
    public class ProcessingRegistrations
    {
        public static string I01 = "01 Registration";
        public static string I02 = "02 Expiry date";
        public static string I03 = "03 Name";
        public static string I04 = "04 Address";
        public static string I05 = "05 Postal";
        public static string I06 = "06 Name";
        public static string I08 = "08 Reproduction";
        public static string I09 = "09 Reproduction";
        public static string I12 = "12 In case";
        public static string I13 = "13 Classification";
        public static string I14 = "14 Enumeration";


        public static DirectoryInfo processed;

        public static List<Registrations> Registrations(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Registrations> elementsOut = new List<Registrations>();

            if(elements != null && elements.Count > 0)
            {
                List<string> splittedRecord = new List<string>();
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                for(int i = 0; i < elements.Count; i++)
                {
                    if(elements[i].Name.LocalName == "Page")
                    {
                        pgnr = elements[i].Attribute("number").Value;
                    }
                    var value = elements[i].Value;
                    if (value.StartsWith(I01))
                    {
                        var currentElement = new Registrations();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Replace("_", "").Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.StartsWith(I01));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue, new string[] { I01, I02, I03, I04, I05, I06, I08, I09, I12, I13, I14 });
                        }

                        foreach (var record in splittedRecord)
                        {
                            if (record.StartsWith(I08))
                            {
                                var text = record.Replace("\n", " ")
                                    .Replace("08 Reproduction of word mark", "")
                                    .Trim();
                                currentElement.TMNM = text;
                                if (!string.IsNullOrEmpty(currentElement.TMNM))
                                {
                                    currentElement.TMTY = "W";
                                }
                            }
                            if (record.StartsWith(I09))
                            {
                                currentElement.TMTY = "M";
                            }
                            if (record.StartsWith(I01))
                            {
                                var temp = record.Replace("\n", " ");
                                var index = temp.IndexOf("Number and date");
                                var RGNR = temp.Substring(0, index).Replace(I01, "").Replace("number", "").Trim();
                                var text = temp.Substring(index).Replace("Number and date of receipt (day and hour) of the filing", "").Trim();
                                var matchAPNR = Regex.Match(text, @"[A-Z]{1}-\d+");
                                var APNR = matchAPNR.Value;
                                var APDA = text.Replace(APNR, "").Trim();
                                APDA = APDA.Substring(0, APDA.IndexOf(',')).Trim();
                                var matchWord = Regex.Match(APDA, "[^0-9]+");
                                text = Methods.ToDate(matchWord.Value);
                                var split = APDA.Split(matchWord.Value);
                                if(split[0].Length == 1)
                                {
                                    APDA = "0" + APDA;
                                }
                                APDA = APDA.Replace(matchWord.Value, text);
                                APDA = Methods.DateNormalize(APDA);
                                currentElement.APDA = APDA;
                                currentElement.APNR = APNR;
                                currentElement.RGNR = RGNR;
                                processed = Directory.CreateDirectory(Path.Combine(CW_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(CW_main.currentFile.FullName) + "\\Registrations"));
                                if (lastImageId != null)
                                {
                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(CW_main.currentFile.DirectoryName, Images[lastImageId]);
                                    if (File.Exists(fileName))
                                        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR.Replace("D", "").Replace("-", "")) + ext); }
                                        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                }
                            }
                            if (record.StartsWith(I02))
                            {
                                var text = record.Replace(I02, "").Replace("\n", " ").Trim();
                                var match = Regex.Match(text, @"[^0-9]+");
                                var month = Methods.ToDate(match.Value);
                                var split = text.Split(match.Value);
                                if (split[0].Length == 1)
                                {
                                    text = "0" + text;
                                }
                                text = text.Replace(match.Value, month);
                                text = Methods.DateNormalize(text);
                                currentElement.EXDA = text;
                            }
                            if (record.StartsWith(I03))
                            {
                                currentElement.OWNN = record.Replace("\n", " ").Replace($"{I03} of the depositor", "").Trim();
                            }
                            if (record.StartsWith(I04))
                            {
                                var text = record.Replace("\n", " ");
                                currentElement.OWNA = text.Replace("04 Address (street and number) of the depositor", "").Replace("05 Postal code and place of the depositor", "").Trim();
                                currentElement.OWNC = Methods.ToOWNC(currentElement.OWNA);
                            }
                            if (record.StartsWith(I05))
                            {
                                var text = record.Replace("\n", " ").Replace("05 Postal code and place of the depositor", "").Trim();
                                currentElement.OWNA = currentElement.OWNA + text;
                                currentElement.OWNC = Methods.ToOWNC(currentElement.OWNA);
                            }
                            if (record.StartsWith(I06))
                            {
                                var text = record.Replace("06 Name and address of the trademark agent or", "")
                                    .Replace("address for correspondence of the depositor", "").Trim();
                                var split = text.Split('\n');
                                currentElement.CORN = split[0].Trim();
                                text = text.Replace(currentElement.CORN, "").Trim();
                                currentElement.CORA = text.Replace("\n", " ");
                                currentElement.CORC = Methods.ToOWNC(text);
                            }
                            if (record.StartsWith(I13))
                            {
                                List<string> classes = new List<string>();
                                List<string> descs = new List<string>();

                                var text = record.Replace("\n", " ").Replace("13 Classification and specification of goods and services", "").Trim();
                                var index = text.IndexOf("14");
                                if (index != -1)
                                {
                                    text = text.Substring(0, index);
                                }
                                var pattern = new Regex(@"(?<Class>Class\s*[0-9]{2})(?<DESC>.*)");

                                var matches = pattern.Matches(text);
                                foreach(Match match in matches)
                                {
                                    classes.Add(match.Groups["Class"].Value.Replace("Class", "").Trim());
                                    descs.Add(match.Groups["DESC"].Value.Trim(' ', ':', ' '));
                                }
                                currentElement.DESC = descs;
                                currentElement.CLAS = classes;
                            }
                            currentElement.PGNR = pgnr;
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

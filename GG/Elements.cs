﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GG
{
    public class Elements
    {
        public class Applications
        {
            public string APDA { get; set; }
            public string RGNR { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string CORN { get; set; }
            public string NOTE { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string PGNR { get; set; }
        }
    }
}

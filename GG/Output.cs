﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static GG.Elements;

namespace GG
{
    public class Output
    {
        public static void ApplicationsToFile(List<Applications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_App.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("RGNR:\t" + record.RGNR);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("NOTE:\tDESC missing");
                        if (record.CORN != null && !string.IsNullOrEmpty(record.CORN))
                        {
                            sf.WriteLine("CORN:\t" + record.CORN);
                        }
                        for (int i = 0; i < record.CLAS.Count; i++)
                        {
                            sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                            sf.WriteLine("DESC:\tS");
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tRGNR:\t" + record.RGNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

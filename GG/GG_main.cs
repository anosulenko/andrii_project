﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GG
{
    class GG_main
    {
        public static DirectoryInfo PathToTetml = new DirectoryInfo(@"C:\Users\Razrab\Desktop\GG\");
        public static FileInfo currentFile = null;
        static void Main(string[] args)
        {
            var files = new List<string>();
            foreach (FileInfo file in PathToTetml.GetFiles("*.tetml", SearchOption.AllDirectories))
                files.Add(file.FullName);

            XElement elem = null;
            List<XElement> eAppList = new List<XElement>(); // applications elements
            List<XElement> imageList = new List<XElement>(); //images elements
            Dictionary<string, string> Images = new Dictionary<string, string>();

            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                elem = XElement.Load(file);
                eAppList = elem.Descendants().Where(e => e.Name.LocalName == "Text" || e.Name.LocalName == "PlacedImage" || e.Name.LocalName == "Page")
                    .ToList();
                imageList = elem.Descendants().Where(i => i.Name.LocalName == "Image")
                    .ToList();

                foreach (var image in imageList)
                {
                    var value = image.Attribute("id")?.Value;
                    if (value != null)
                    {
                        var value1 = image.Attribute("filename")?.Value;
                        Images.Add(value, image.Attribute("filename")?.Value);
                    }
                }

                if (eAppList.Count > 0)
                {
                    var processedRecords = Processing.Applications(eAppList, Images);
                    Output.ApplicationsToFile(processedRecords, Processing.processed);
                }
                else
                    Console.WriteLine("Reg elements was not found");
            }
        }
    }
}

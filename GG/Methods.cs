﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GG
{
    public class Methods
    {
        public static bool IsImage { get; set; }
        public static List<string> RecSplit(string s)
        {
            List<string> splittedRecords = new List<string>();

            s = s.Substring(0, s.IndexOf("PRIORITY CLAIM")).Trim();
            var match = Regex.Match(s, "APPLICANT:.*AGENT:", RegexOptions.Singleline).Value;
            match = match.Replace("AGENT:", "").Trim();
            s = Regex.Replace(s, match, "", RegexOptions.Singleline);
            splittedRecords = s.Split('\n').ToList();

            if (!splittedRecords.Last().Contains("AGENT"))
            {
                for (int i = 0; i < splittedRecords.Count; i++)
                {
                    if (splittedRecords[i].Contains("AGENT"))
                    {
                        splittedRecords[i] = splittedRecords[i] + "\n" + splittedRecords.Last();
                    }
                    if (i == splittedRecords.Count - 1)
                    {
                        splittedRecords[i] = "";
                    }
                }
                IsImage = true;
            }
            else
                IsImage = false;

            splittedRecords.Add(match);

            return splittedRecords;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            var date = Regex.Match(s, @"(?<day>\d{2})[^0-9]*(?<month>\d{2})[^0-9]*(?<year>\d{4})");
            dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            return dateNormalized;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;
using static GG.Elements;

namespace GG
{
    public class Processing
    {
        private static string APDA = "FILING DATE";
        private static string RGNR = "SUBMISSION REFERENCE NUMBER";
        private static string OWNN = "APPLICANT";
        private static string CORN = "AGENT";
        private static string NOTE = "NOTE";
        private static string CLAS = "CLASSES";

        public static DirectoryInfo processed;
        public static List<Applications> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if (elements != null && elements.Count > 0)
            {
                List<string> splittedRecord = new List<string>();
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].Name.LocalName == "Page")
                    {
                        pgnr = elements[i].Attribute("number").Value;
                    }
                    var value = elements[i].Value;
                    if (value.StartsWith(APDA))
                    {
                        var currentElement = new Applications();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.StartsWith(APDA));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }

                        foreach (var record in splittedRecord)
                        {
                            if (record.StartsWith(APDA))
                            {
                                currentElement.APDA = Methods.DateNormalize(record.Replace($"{APDA}:", "").Trim());
                            }
                            if (record.StartsWith(RGNR))
                            {
                                currentElement.RGNR = record.Replace($"{RGNR}:", "").Trim();
                                processed = Directory.CreateDirectory(Path.Combine(GG_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(GG_main.currentFile.FullName) + "\\Applications"));
                                if (lastImageId != null)
                                {
                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(GG_main.currentFile.DirectoryName, Images[lastImageId]);
                                    if (File.Exists(fileName))
                                        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.RGNR) + ext); }
                                        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                }
                            }
                            if (record.StartsWith(OWNN))
                            {
                                currentElement.OWNN = record.Replace("\n", " ").Replace($"{OWNN}:", "").Trim();
                            }
                            if (record.StartsWith(CORN))
                            {
                                if (Methods.IsImage == true)
                                {
                                    var split = record.Split('\n');
                                    currentElement.CORN = split[0].Replace($"{CORN}:", "").Trim();
                                    currentElement.TMNM = split[1];
                                    currentElement.TMTY = "W";
                                }
                                else
                                {
                                    currentElement.CORN = record.Replace($"{CORN}:", "").Trim();
                                    currentElement.TMTY = "M";
                                }
                            }
                            if (record.StartsWith(CLAS))
                            {
                                List<string> classes = new List<string>();

                                var split = record.Split(',');
                                foreach(var item in split)
                                {
                                    classes.Add(item.Replace("CLASSES:", "").Trim());
                                }
                                currentElement.CLAS = classes;
                            }
                            currentElement.PGNR = pgnr;
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

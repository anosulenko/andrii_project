﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MA_WF
{
    public class ProcessApp
    {
        static readonly string I151 = "(151)"; //RGDA
        static readonly string I180 = "(180)"; //EXDA
        static readonly string I732 = "(732)";
        static readonly string I511 = "(511)";
        static readonly string I300 = "(300)";

        public static List<XElement> regList = null;
        public static FileInfo currentFile = null;

        public struct ClassDesc
        {
            public string CLAS { get; set; }
            public string DESC { get; set; }
        }
        public class ElementsForOutput
        {
            public string APNR { get; set; }
            public string RGNR { get; set; }
            public string RGDA { get; set; }
            public string EXDA { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string OWNC { get; set; }
            public string OWNA { get; set; }
            public string PrioNumber { get; set; }
            public string PrioDate { get; set; }
            public string PrioCountry { get; set; }
            public List<ClassDesc> ListClassDesc { get; set; }
        }

        static List<ElementsForOutput> ElementsOut = new List<ElementsForOutput>();

        public static void AppProcess()
        {
            var dir = new DirectoryInfo(MA.pdfDirectoryInfo);
            var outputFolderPath = MA.pdfDirectoryInfo.Replace("Input", "Output");
            var files = new List<string>();
            foreach (FileInfo file in dir.GetFiles("*.tetml", SearchOption.AllDirectories))
            {
                if (!Regex.IsMatch(file.Name, @"_Output.txt"))
                    files.Add(file.FullName);
            }
            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                string fileName = file;
                XElement tet = XElement.Load(fileName);
                regList = tet.Descendants().Where(d => d.Name.LocalName == "Text").ToList();
                ElementsOut.Clear();
                Directory.CreateDirectory(outputFolderPath);
                string path = Path.Combine(outputFolderPath, Path.GetFileNameWithoutExtension(file) + "_Output.txt");
                StreamWriter sf = new StreamWriter(path);
                ElementsForOutput currentElement = null;

                Regex pat = new Regex(@"^\d{6}$");
                string[] splittedRecord = null;
                string currentPageNumber = null;
                string tmpRecordValue;
                int tmpInc;
                for (int i = 0; i < regList.Count; ++i)
                {
                    var tmp1 = regList[i].Name.LocalName;
                    var value = regList[i].Value;
                    if (pat.Match(value).Success)
                    {
                        currentElement = new ElementsForOutput();
                        ElementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            tmpRecordValue += regList[tmpInc].Value + "\n";
                            ++tmpInc;
                        } while (tmpInc < regList.Count() && !pat.Match(regList[tmpInc].Value).Success);

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }
                        foreach (var record in splittedRecord)
                        {
                            if (pat.Match(record).Success)
                            {
                                currentElement.APNR = pat.Match(record).Value.Trim();
                                currentElement.RGNR = pat.Match(record).Value.Trim();
                            }
                            if (record.StartsWith(I151))
                            {
                                currentElement.RGDA = Methods.DateNormalize(record.Replace(I151, "").Trim());
                            }
                            if (record.StartsWith(I180))
                            {
                                currentElement.EXDA = Methods.DateNormalize(record.Replace(I180, "").Trim());
                            }
                            if (record.StartsWith(I732))
                            {
                                var owner = Methods.OwnerProcess(record.Replace(I732, "").Trim());
                                if (owner.Name == null) Console.WriteLine("Eror in owner identification:\t" + currentElement.APNR);
                                currentElement.OWNN = owner.Name;
                                currentElement.OWNA = owner.Address;
                                currentElement.OWNC = owner.Country;
                            }
                            if (record.StartsWith(I511))
                            {
                                currentElement.ListClassDesc = Methods.DescClass(record.Replace(I511, "").Trim());
                            }
                            if (record.StartsWith(I300))
                            {
                                var tmp = record.Replace(I300, "").Trim();
                                var p = new Regex(@"(?<country>^[A-Z]{2})\s*,*\s*(?<date>\d{4}-\d{2}-\d{2}).*,(?<number>.*$)");
                                if (tmp != "")
                                {
                                    var rx = p.Match(tmp);
                                    if (rx.Success)
                                    {
                                        currentElement.PrioNumber = rx.Groups["number"].Value.Trim();
                                        currentElement.PrioDate = rx.Groups["date"].Value.Trim();
                                        currentElement.PrioCountry = rx.Groups["country"].Value.Trim();
                                    }
                                    else
                                    {
                                        Console.WriteLine("Priority identification error:\t" + currentElement.APNR);
                                    }
                                }
                            }
                        }
                    }
                }


                if (ElementsOut != null)
                {
                    foreach (var elemOut in ElementsOut)
                    {
                        try
                        {
                            sf.WriteLine("****");
                            sf.WriteLine("APNR:\t" + elemOut.APNR);
                            sf.WriteLine("RGNR:\t" + elemOut.RGNR);
                            sf.WriteLine("APDA:\t" + elemOut.RGDA);
                            sf.WriteLine("EXDA:\t" + elemOut.EXDA);
                            sf.WriteLine("TMNM:\t");
                            sf.WriteLine("TMTY:\tM");
                            if (elemOut.PrioCountry != null) sf.WriteLine("PRIC:\t" + elemOut.PrioCountry);
                            if (elemOut.PrioNumber != null) sf.WriteLine("PRIN:\t" + elemOut.PrioNumber);
                            if (elemOut.PrioDate != null) sf.WriteLine("PRID:\t" + elemOut.PrioDate);
                            sf.WriteLine("OWNN:\t" + elemOut.OWNN);
                            if (elemOut.OWNA != "") sf.WriteLine("OWNA:\t" + elemOut.OWNA);
                            if (elemOut.OWNC != "") sf.WriteLine("OWNC:\t" + elemOut.OWNC);
                            for (int i = 0; i < elemOut.ListClassDesc.Count(); i++)
                            {
                                sf.WriteLine("CLAS:\t" + elemOut.ListClassDesc[i].CLAS);
                                sf.WriteLine("DESC:\t" + elemOut.ListClassDesc[i].DESC);
                            }
                        }
                        catch(Exception e)
                        {

                        }
                    }
                }

                sf.Flush();
                sf.Close();
            }
        }
    }
}

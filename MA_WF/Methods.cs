﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MA_WF
{
    public class Methods
    {
        public static string[] RecSplit(string recString)
        {
            string[] splittedRecord = null;
            string tempStrC = recString.Trim();
            if (tempStrC != "")
            {
                if (tempStrC.Contains("("))
                {
                    Regex regexPatOne = new Regex(@"\(\d{3}\)", RegexOptions.IgnoreCase);
                    MatchCollection matchesClass = regexPatOne.Matches(recString);
                    if (matchesClass.Count > 0)
                    {
                        foreach (Match matchC in matchesClass)
                        {
                            tempStrC = tempStrC.Replace(matchC.Value, "***" + matchC.Value);
                        }
                    }
                }
                /*Splitting record*/
                splittedRecord = tempStrC.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            }
            return splittedRecord;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            }
            return dateNormalized;
        }

        public static string ClassNormalize(string s)
        {
            if (s.Trim().Length == 1) return 0 + s;
            else return s;
        }
        public static List<ProcessApp.ClassDesc> DescClass(string s)
        {
            s = s.Replace("\n", " ");
            List<ProcessApp.ClassDesc> tmpValues = new List<ProcessApp.ClassDesc>();
            List<ProcessApp.ClassDesc> tmpValuesSorted = new List<ProcessApp.ClassDesc>();

            var splitPat = new Regex(@"(?=\b\d+\b)");
            var clasDescPat = new Regex(@"(?<class>\d+\b)(?<desc>.*$)");
            try
            {
                var splString = splitPat.Split(s).Where(x => x != "").ToList();
                if (splString.Count > 0)
                {
                    foreach (var record in splString)
                    {
                        var _ = clasDescPat.Match(record);
                        tmpValues.Add(new ProcessApp.ClassDesc
                        {
                            CLAS = ClassNormalize(_.Groups["class"].Value),
                            DESC = _.Groups["desc"].Value.Trim()
                        });
                    }
                }
                /*Sort and merge description by class value*/
                /* Get uniq class values*/
                var uniqClasses = tmpValues.Select(x => x.CLAS).ToList().Distinct();
                foreach (var item in uniqClasses)
                {
                    tmpValuesSorted.Add(new ProcessApp.ClassDesc
                    {
                        CLAS = item,
                        DESC = string.Join(" ", tmpValues.Where(x => x.CLAS == item).Select(x => x.DESC).ToList()) // Select all descriptions with same classes and merge them under single class value
                    });
                }
                Console.WriteLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Something went wrong in Desc and Class processing function!");
            }
            return tmpValuesSorted;
        }

        public class OwnerStruct
        {
            public string Name { get; set; }
            public string Address { get; set; }
            public string Country { get; set; }
        }
        public static OwnerStruct OwnerProcess(string s)
        {
            Regex pat = new Regex(@"(?<name>^[^\n]+)\n(?<address>.*)\n(?<country>[A-Z]{2}$)", RegexOptions.Singleline);
            var owner = new OwnerStruct();
            var a = pat.Match(s.Trim());
            if (a.Success)
            {
                owner.Name = a.Groups["name"].Value.Trim();
                owner.Address = a.Groups["address"].Value.Replace("\n", " ").Trim();
                owner.Country = a.Groups["country"].Value.Trim();
            }
            return owner;
        }
    }
}

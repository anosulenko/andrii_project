﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using static HN.Elements;

namespace HN
{
    public class Processing
    {
        public static Regex APNR = new Regex(@"(1/|\[1\])\s?(No\.)?\s?Solicitud:", RegexOptions.IgnoreCase);
        public static Regex APDA = new Regex(@"(2/|\[2\])\s?Fecha de presentación:", RegexOptions.IgnoreCase);
        public static Regex RGDA = new Regex(@"(11/|\[11\])\s?Fecha de emisión:", RegexOptions.IgnoreCase);
        public static Regex TMNM = new Regex(@"(6/|\[6\])\s?Denominación", RegexOptions.IgnoreCase);
        public static Regex OWNN = new Regex(@"(4/|\[4\])\s?Solicitante:", RegexOptions.IgnoreCase);
        public static Regex OWNA = new Regex(@"(4(\.|\/)1/|\[4\.1\])\s?Domicilio:", RegexOptions.IgnoreCase);
        public static Regex CORN = new Regex(@"(9/|\[9\])\s?Nombre:", RegexOptions.IgnoreCase);
        public static Regex CLAS = new Regex(@"(7/|\[7\])\s?Clase Internacional:", RegexOptions.IgnoreCase);
        public static Regex DESC = new Regex(@"(8/|\[8\])\s?Protege y distingue:", RegexOptions.IgnoreCase);

        public static Regex Field3 = new Regex(@"(3/|\[3\])\s?Solicitud de (r|R)egistro de:", RegexOptions.IgnoreCase);
        public static Regex Field42 = new Regex(@"(4\.2/|\[4\.2\])\s?Organizada bajo las Leyes de:", RegexOptions.IgnoreCase);
        public static Regex Field5 = new Regex(@"(5/|\[5\])\s?Registro Básico:", RegexOptions.IgnoreCase);
        public static Regex Field62 = new Regex(@"(6\.2\/|\[6\.2\])\s?Reivindicaciones:", RegexOptions.IgnoreCase);
        public static Regex Field81 = new Regex(@"(8\.1\/|\[8\.1\])\s?Página Adicional:?", RegexOptions.IgnoreCase);
        public static Regex Field10 = new Regex(@"(10\/|\[10\])\s?Nombre:", RegexOptions.IgnoreCase);
        public static Regex Field12 = new Regex(@"(12\/|\[12\])\s?Reservas:", RegexOptions.IgnoreCase);
        public static Regex Distictivo = new Regex(@"(6\.1\/|\[6\.1\])\s?Distintivo:", RegexOptions.IgnoreCase);

        public static DirectoryInfo processed;
        public static List<Registrations> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Registrations> elementsOut = new List<Registrations>();

            if (elements != null && elements.Count > 0)
            {
                List<string> splittedRecord = new List<string>();
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].Name.LocalName == "Page")
                    {
                        pgnr = elements[i].Attribute("number").Value;
                    }
                    var value = elements[i].Value;
                    if (!string.IsNullOrEmpty(APNR.Match(value).Value) && value.StartsWith(APNR.Match(value).Value))
                    {
                        var currentElement = new Registrations();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        var start = "";

                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Trim() + "\n";
                            }
                            ++tmpInc;
                            if(tmpInc <= elements.Count() - 1)
                            {
                                start = APNR.Match(elements[tmpInc].Value).Value;
                            }
                        } while (tmpInc < elements.Count && string.IsNullOrEmpty(start));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue, new string[] { APNR.Match(tmpRecordValue).Value, APDA.Match(tmpRecordValue).Value, RGDA.Match(tmpRecordValue).Value,
                                TMNM.Match(tmpRecordValue).Value, OWNN.Match(tmpRecordValue).Value, OWNA.Match(tmpRecordValue).Value, CORN.Match(tmpRecordValue).Value, CLAS.Match(tmpRecordValue).Value,
                                DESC.Match(tmpRecordValue).Value, Field3.Match(tmpRecordValue).Value, Field42.Match(tmpRecordValue).Value, Field5.Match(tmpRecordValue).Value,
                                Field62.Match(tmpRecordValue).Value, Field81.Match(tmpRecordValue).Value, Field10.Match(tmpRecordValue).Value, Field12.Match(tmpRecordValue).Value});
                        }

                        foreach (var record in splittedRecord)
                        {
                            if (!string.IsNullOrEmpty(APNR.Match(record).Value))
                            {
                                currentElement.APNR = Methods.ToAPNR(record.Replace(APNR.Match(record).Value, "").Trim());
                                processed = Directory.CreateDirectory(Path.Combine(HN_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(HN_main.currentFile.FullName) + "\\Registrations"));
                                if (lastImageId != null)
                                {
                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(HN_main.currentFile.DirectoryName, Images[lastImageId]);
                                    if (File.Exists(fileName))
                                        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR) + ext); }
                                        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                }
                            }

                            if (!string.IsNullOrEmpty(APDA.Match(record).Value))
                            {
                                currentElement.APDA = Methods.ToDate(record.Replace(APDA.Match(record).Value, "").Trim());
                            }

                            if (!string.IsNullOrEmpty(RGDA.Match(record).Value))
                            {
                                currentElement.RGDA = Methods.ToDate(record.Replace(RGDA.Match(record).Value, "").Trim());
                            }

                            if (!string.IsNullOrEmpty(TMNM.Match(record).Value))
                            {
                                var index = record.IndexOf(Distictivo.Match(record).Value);
                                var image = record.Substring(index).Replace(Distictivo.Match(record).Value, "").Trim();
                                if (!string.IsNullOrEmpty(image))
                                {
                                    currentElement.TMNM = image.Replace("\n", " ");
                                }
                                currentElement.TMTY = "M";
                            }

                            if (!string.IsNullOrEmpty(OWNN.Match(record).Value))
                            {
                                currentElement.OWNN = record.Replace(OWNN.Match(record).Value, "").Replace("\n", " ").Trim();
                            }

                            if (!string.IsNullOrEmpty(OWNA.Match(record).Value))
                            {
                                currentElement.OWNA = record.Replace(OWNA.Match(record).Value, "").Replace("\n", " ").Trim();
                                currentElement.OWNC = Methods.ToOWNC(currentElement.OWNA);
                            }

                            if (!string.IsNullOrEmpty(CORN.Match(record).Value))
                            {
                                var index = record.IndexOf("Lo que");
                                var text = record;
                                if (index > 0)
                                    text = text.Substring(0, index);
                                currentElement.CORN = text.Replace(CORN.Match(record).Value, "").Replace("\n", " ").Replace("E.- SUSTITUYE PODER", "").Trim();
                            }

                            if (!string.IsNullOrEmpty(CLAS.Match(record).Value) && string.IsNullOrEmpty(currentElement.CLAS))
                            {
                                var matchClass = Regex.Match(record, @"(7/|\[7\])\s?Clase Internacional:\s*\d{1,2}").Value;
                                matchClass = matchClass.Replace(CLAS.Match(record).Value, "").Trim();
                                currentElement.CLAS = matchClass;
                                if (currentElement.CLAS.Length == 1)
                                    currentElement.CLAS = "0" + currentElement.CLAS;

                                if(currentElement.CLAS == "0")
                                {
                                    currentElement.CLAS = "99";
                                }
                            }

                            if (!string.IsNullOrEmpty(DESC.Match(record).Value))
                            {
                                currentElement.DESC = record.Replace(DESC.Match(record).Value, "").Replace("\n", " ").Replace("D.- APODERADO LEGAL", "").Trim();
                            }
                            currentElement.PGNR = pgnr;
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

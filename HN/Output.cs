﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static HN.Elements;

namespace HN
{
    public class Output
    {
        public static void RegistrationsToFile(List<Registrations> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_App.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("RGDA:\t" + record.RGDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        sf.WriteLine("CORN:\t" + record.CORN);
                        sf.WriteLine("CLAS:\t" + record.CLAS);
                        sf.WriteLine("DESC:\t" + record.DESC);
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

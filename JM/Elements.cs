﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JM
{
    public class Elements
    {
        public class Removed
        {
            public string APNR { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string RMDA { get; set; }
            public string NOTE { get; set; }
            public string PGNR { get; set; }
        }

        public class Unpaidrenewals
        {
            public string APNR { get; set; }
            public string EXDA { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string NOTE { get; set; }
            public string PGNR { get; set; }
        }

        public class Renewals
        {
            public string APNR { get; set; }
            public string EXDA { get; set; }
            public string OWNN { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string NOTE { get; set; }
            public string PGNR { get; set; }
        }

        public class Applications
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string PRIC { get; set; }
            public string PRIN { get; set; }
            public string PRID { get; set; }
            public List<string> CLAS { get; set; }
            public List<string> DESC { get; set; }
            public string PGNR { get; set; }
        }
    }
}

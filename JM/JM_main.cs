﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Linq;

namespace JM
{
    class JM_main
    {
        public static DirectoryInfo PathToTetml = new DirectoryInfo(@"C:\Users\Razrab\Desktop\JM\");
        public static FileInfo currentFile = null;
        public static DirectoryInfo processed = null;
        public static string currentFileName = null;
        static void Main(string[] args)
        {
            var files = new List<string>();
            foreach (FileInfo file in PathToTetml.GetFiles("*.htm", SearchOption.AllDirectories))
                files.Add(file.FullName);

            XElement elem = null;
            List<XElement> eRegList = new List<XElement>();
            List<XElement> imageList = new List<XElement>();
            Dictionary<string, string> Images = new Dictionary<string, string>();

            foreach (var file in files)
            {
                currentFile = new FileInfo(file);
                currentFileName = Path.GetFileName(file);
                var text = File.ReadAllText(file);

                if (!string.IsNullOrEmpty(text))
                {
                    if (currentFileName.Contains("Removed"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName)));
                        var processedRecords = Processing.Removed(text);
                        Output.RemovedToFile(processedRecords, processed);
                    }
                    else if (currentFileName.Contains("Unpaidrenewal"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName)));
                        var processedRecords = Processing.Unpaidrenewals(text);
                        Output.Unpaidrenewals(processedRecords, processed);
                    }
                    else if (currentFileName.Contains("Renewals"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName)));
                        var processedRecords = Processing.Renewals(text);
                        Output.Renewals(processedRecords, processed);
                    }
                    else if (currentFileName.Contains("Applications"))
                    {
                        processed = Directory.CreateDirectory(Path.Combine(PathToTetml.FullName, Path.GetFileNameWithoutExtension(currentFile.FullName)));
                        var processedRecords = Processing.Applications(text);
                        Output.Applications(processedRecords, processed);
                    }
                }
                else
                    Console.WriteLine("Reg elements was not found");
            }
        }
    }
}

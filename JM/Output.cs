﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static JM.Elements;

namespace JM
{
    public class Output
    {
        public static void RemovedToFile(List<Removed> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Removed.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("RMDA:\t" + record.RMDA);
                        sf.WriteLine("NOTE:\t" + record.NOTE);
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void Unpaidrenewals(List<Unpaidrenewals> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Unpaidrenewals.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("NOTE:\t" + record.NOTE);
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }

                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void Renewals(List<Renewals> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Unpaidrenewals.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("NOTE:\t" + record.NOTE);
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }

        public static void Applications(List<Applications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_Applications.txt");
            var sf = new StreamWriter(path);
            if(output != null)
            {
                foreach(var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        sf.WriteLine("CORN:\t" + record.CORN);
                        sf.WriteLine("CORA:\t" + record.CORA);
                        sf.WriteLine("CORC:\t" + record.CORC);
                        if (!string.IsNullOrEmpty(record.PRIN))
                        {
                            sf.WriteLine("PRIC:\t" + record.PRIC);
                            sf.WriteLine("PRIN:\t" + record.PRIN);
                            sf.WriteLine("PRID:\t" + record.PRID);
                        }
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

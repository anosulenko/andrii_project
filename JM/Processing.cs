﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static JM.Elements;

namespace JM
{
    class Processing
    {
        private static string APNR = "Application#";
        private static string APDA = "Filing Date";
        private static string OWN = "Applicant";
        private static string COR = "Agent/Address for Service";
        private static string PRI = "Priority";
        private static string FT = "Class Details";
        private static string Colour = "Colour";
        private static string Translation = "Translation";
        private static string Disclaimer = "Disclaimer";
        private static string Series = "Series";

        public static List<Removed> Removed(string input)
        {
            List<Removed> elementsOut = new List<Removed>();

            var elements = input.Split("\r\n\r\n");

            foreach(var elem in elements)
            {
                var currentElement = new Removed();
                var text = elem.Trim();
                var number = Regex.Match(text, @"\d{6}");
                var strings = text.Split("\n");
                var date = strings.Last();
                var monthMatch = Regex.Match(date, @"\p{L}+").Value;
                var month = ToMonth(monthMatch);
                text = text.Replace(number.Value, "").Replace(date, "").Trim();
                date = date.Replace(monthMatch, month);
                var @class = Regex.Match(text, @"(\d{1,2}\W{0,2}(and)?\s*)+");
                text = text.Replace(@class.Value, "");
                var classes = @class.Value.Replace("and", "").Replace(",", " ").Replace("\n", " ").Trim().Split(' ');
                var Classes = new List<string>();
                var descs = new List<string>();

                for(int i = 0; i < classes.Count(); i++)
                {
                    classes[i] = classes[i].Trim();
                    if (!string.IsNullOrEmpty(classes[i]))
                    {
                        Classes.Add(classes[i].Trim());
                        descs.Add("S");
                    }
                }

                currentElement.APNR = number.Value;
                currentElement.DESC = descs;
                currentElement.CLAS = Classes;
                currentElement.RMDA = DateNormalize(date);
                currentElement.NOTE = "DESC missing";

                if (!string.IsNullOrEmpty(currentElement.RMDA))
                {
                    Console.WriteLine($"{currentElement.APNR} RMDA is missing");
                }
                elementsOut.Add(currentElement);
            }

            return elementsOut;
        }

        public static List<Unpaidrenewals> Unpaidrenewals(string input)
        {
            List<Unpaidrenewals> elementsOut = new List<Unpaidrenewals>();

            var elements = input.Split("\r\n\r\n");

            foreach (var elem in elements)
            {
                var currentElement = new Unpaidrenewals();
                var text = elem.Trim();
                var number = Regex.Match(text, @"\d{6}");
                var strings = text.Split("\n");
                var date = strings.Last();
                var monthMatch = Regex.Match(date, @"\p{L}+").Value;
                var month = ToMonth(monthMatch);
                text = text.Replace(number.Value, "").Replace(date, "").Trim();
                date = date.Replace(monthMatch, month);
                var @class = Regex.Match(text, @"(\d{1,2}\W{0,2}(and)?\s*)+");
                text = text.Replace(@class.Value, "");
                var classes = @class.Value.Replace("and", "").Replace(",", " ").Replace("\n", " ").Trim().Split(' ');
                var Classes = new List<string>();
                var descs = new List<string>();

                for (int i = 0; i < classes.Count(); i++)
                {
                    classes[i] = classes[i].Trim();
                    if (!string.IsNullOrEmpty(classes[i]))
                    {
                        Classes.Add(classes[i].Trim());
                        descs.Add("S");
                    }
                }

                currentElement.APNR = number.Value;
                currentElement.DESC = descs;
                currentElement.CLAS = Classes;
                currentElement.EXDA = DateNormalize(date);
                currentElement.NOTE = "DESC missing";

                if (!string.IsNullOrEmpty(currentElement.EXDA))
                {
                    Console.WriteLine($"{currentElement.APNR} EXDA is missing");
                }
                elementsOut.Add(currentElement);
            }

            return elementsOut;
        }

        public static List<Renewals> Renewals(string input)
        {
            List<Renewals> elementsOut = new List<Renewals>();

            var elements = input.Split("\r\n\r\n");
            var pgnr = 1;
            foreach (var elem in elements)
            {
                if (!elem.Contains("Trade Mark") && !elem.Contains("Industrial Property"))
                {
                    var currentElement = new Renewals();
                    var text = elem.Trim();
                    var number = Regex.Match(text, @"\d{6}");
                    var strings = text.Split("\n");
                    var date = strings.Last();
                    var monthMatch = Regex.Match(date, @"\p{L}+").Value;
                    var month = ToMonth(monthMatch);
                    text = text.Replace(number.Value, "").Replace(date, "").Trim();
                    date = date.Replace(monthMatch, month);
                    var @class = Regex.Match(text, @"(\d{1,2}\W{0,2}(and)?\s*)+");
                    text = text.Replace(@class.Value, "");
                    var classes = @class.Value.Replace("and", "").Replace(",", " ").Replace("\n", " ").Trim().Split(' ');
                    var Classes = new List<string>();
                    var descs = new List<string>();

                    for (int i = 0; i < classes.Count(); i++)
                    {
                        classes[i] = classes[i].Trim();
                        if (!string.IsNullOrEmpty(classes[i]))
                        {
                            Classes.Add(classes[i].Trim());
                            descs.Add("S");
                        }
                    }

                    currentElement.APNR = number.Value;
                    currentElement.DESC = descs;
                    currentElement.CLAS = Classes;
                    currentElement.EXDA = DateNormalize(date);
                    currentElement.NOTE = "DESC missing";
                    currentElement.OWNN = text.Replace("\n", " ").Trim();
                    currentElement.PGNR = $"{pgnr}";

                    if (!string.IsNullOrEmpty(currentElement.EXDA))
                    {
                        Console.WriteLine($"{currentElement.APNR} EXDA is missing");
                    }
                    elementsOut.Add(currentElement);
                }
                else
                    pgnr++;
            }

            return elementsOut;
        }

        public static List<Applications> Applications(string input)
        {
            List<Applications> elementsOut = new List<Applications>();

            var elements = input.Split('\n');
            var pgnr = 1;
            for(int i = 0; i < elements.Count(); i++)
            {
                var currentElement = new Applications();
                if (elements[i].StartsWith("Application#"))
                {
                    string tmpRecordValue = "";
                    var checkPage = "";
                    int tmpInc = i;
                    do
                    {
                        tmpRecordValue += elements[tmpInc] + "\n";
                        ++tmpInc;
                    } while (tmpInc < elements.Count() && !elements[tmpInc].StartsWith("Application#"));

                    var stopValue = tmpInc + 5;

                    checkPage = tmpRecordValue;
                    var matchPage = Regex.Match(checkPage, @"\p{L}+\s*\d{2}\W{0,2}\s*\d{4}\s*.?(J|W)(1|I)PO\s*\d{4}\s*\d{2,3}\s*Vol\.\s*\d{5}\s*Trade\s*Marks\s*Journal\s*No\.\s*\d{3}", RegexOptions.Singleline);
                    if (matchPage.Success)
                    {
                        pgnr++;
                        tmpRecordValue = tmpRecordValue.Replace(matchPage.Value, "");
                    }
                    else
                    {
                        do
                        {
                            checkPage += elements[tmpInc] + "\n";
                            ++tmpInc;
                        } while (tmpInc < stopValue);

                        matchPage = Regex.Match(checkPage, @"\p{L}+\s*\d{2}\W{0,2}\s*\d{4}\s*.?(J|W)(1|I)PO\s*\d{4}\s*\d{2,3}\s*Vol\.\s*\d{5}\s*Trade\s*Marks\s*Journal\s*No\.\s*\d{3}", RegexOptions.Singleline);
                        if (matchPage.Success)
                        {
                            pgnr++;
                        }
                    }

                    var splittedRecords = tmpRecordValue.Split(new string[] { APNR, APDA, OWN, COR, PRI, FT, Colour, Translation, Disclaimer, Series }, StringSplitOptions.RemoveEmptyEntries);

                    try
                    {
                        splittedRecords[0] = APNR + splittedRecords[0];
                        splittedRecords[1] = APDA + splittedRecords[1];
                        splittedRecords[2] = Disclaimer + splittedRecords[2];
                        splittedRecords[3] = PRI + splittedRecords[3];
                        splittedRecords[4] = Translation + splittedRecords[4];
                        splittedRecords[5] = Colour + splittedRecords[5];
                        splittedRecords[6] = FT + splittedRecords[6];
                        splittedRecords[7] = OWN + splittedRecords[7];
                        splittedRecords[8] = COR + splittedRecords[8];
                    }catch(Exception e)
                    {
                        currentElement.APNR = splittedRecords[0];
                    }

                    foreach (var record in splittedRecords)
                    {
                        if (record.StartsWith(APNR))
                        {
                            currentElement.APNR = record.Replace(APNR, "").Trim();
                        }
                        if (record.StartsWith(APDA))
                        {
                            var text = record.Replace(APDA, "").Replace(":", "").Trim();
                            if (!string.IsNullOrEmpty(text))
                            {
                                var matchPic = Regex.Match(record, @"\p{L}+\s*\d{1,2}.?\s*\d{4}\s*\p{L}+", RegexOptions.Singleline);
                                if (matchPic.Success)
                                {
                                    var date = matchPic.Value.Trim().Split('\n').First().Replace(APDA, "").Trim();
                                    var TMNM = matchPic.Value.Trim().Split('\n').Last();

                                    var month = Regex.Match(date, @"\p{L}+").Value;
                                    date = date.Replace(month, "");
                                    month = ToMonth(month);
                                    date = month + " " + date;
                                    currentElement.TMNM = TMNM;
                                    currentElement.TMTY = "W";
                                    currentElement.APDA = DateNormalize(date);
                                }
                                else
                                {
                                    var date = record.Replace(APDA, "").Replace(":", "").Trim();
                                    var month = Regex.Match(date, @"\p{L}+").Value;
                                    date = date.Replace(month, "");
                                    month = ToMonth(month);
                                    date = month + " " + date;
                                    currentElement.APDA = DateNormalize(date);
                                }
                            }
                        }
                        if (record.StartsWith(OWN))
                        {
                            var text = record.Replace(OWN, "").Replace(":", "").Trim();
                            if (!string.IsNullOrEmpty(text))
                            {
                                var strings = record.Replace(OWN, "").Trim().Split('\n');
                                try
                                {
                                    var OWNN = strings[0];
                                    var OWNA = strings[1];
                                    currentElement.OWNA = OWNA.Trim();
                                    currentElement.OWNN = OWNN.Trim();
                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine($"{currentElement.APNR} owner issue: {e}");
                                }
                            }
                        }
                        if (record.StartsWith(COR))
                        {
                            var strings = record.Replace(COR, "").Trim().Split('\n');
                            try
                            {
                                var CORN = strings[0];
                                var CORA = strings[1];
                                currentElement.CORN = CORN.Trim();
                                currentElement.CORA = CORA.Trim();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine($"{currentElement.APNR} agent issue: {e}");
                            }
                        }
                        if (record.StartsWith(PRI))
                        {
                            var text = record.Replace(PRI, "").Replace(":", "").Replace("&nbsp;", "").Trim();
                            var pattern = Regex.Match(text, @"(?<PRIN>\d{7})\s*(?<PRID>\d{2}\/\d{2}\/\d{4})\s*(?<PRIC>\p{L}+ ?\p{L})");

                        }
                        if (record.StartsWith(FT))
                        {
                            List<string> classList = new List<string>();
                            List<string> descList = new List<string>();

                            var text = record.Replace(FT, "").Trim().Trim(':').Trim().Replace("\r\n\r", " ");
                            //var matches = Regex.Matches(record, @"(?<Class>In\s*International\s*Class \d{1,2})\s*(?<Desc>(\p{L}+\s*.{0,3})+\.)", RegexOptions.Singleline);
                            var strings = text.Split("In International Class");
                            foreach (var item in strings)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    classList.Add(item.Trim().Substring(0, 2).Trim());
                                    descList.Add(item.Trim().Substring(2).Trim());
                                }
                                //classList.Add(match.Groups["Class"].Value.Replace("International", "").Replace("In", "").Replace("Class", "").Trim());
                                //descList.Add(match.Groups["Desc"].Value);
                            }

                            currentElement.CLAS = classList;
                            currentElement.DESC = descList;
                        }
                    }
                    if (string.IsNullOrEmpty(currentElement.TMNM))
                    {
                        currentElement.TMTY = "M";
                    }
                    else
                    {
                        currentElement.TMTY = "W";
                    }

                    elementsOut.Add(currentElement);
                }
            }

            return elementsOut;
        }

        public static string ToMonth(string s)
        {
            switch (s)
            {
                case var month when new Regex(@"(January|januari|enero)", RegexOptions.IgnoreCase).Match(month).Success: return "01";
                case var month when new Regex(@"(February|februari|febrero)", RegexOptions.IgnoreCase).Match(month).Success: return "02";
                case var month when new Regex(@"(March|maart|marzo)", RegexOptions.IgnoreCase).Match(month).Success: return "03";
                case var month when new Regex(@"(April|april|abril)", RegexOptions.IgnoreCase).Match(month).Success: return "04";
                case var month when new Regex(@"(May|mei|Mayo)", RegexOptions.IgnoreCase).Match(month).Success: return "05";
                case var month when new Regex(@"(June|juni|junio)", RegexOptions.IgnoreCase).Match(month).Success: return "06";
                case var month when new Regex(@"(July|juli|julio)", RegexOptions.IgnoreCase).Match(month).Success: return "07";
                case var month when new Regex(@"(August|augustus|agosto)", RegexOptions.IgnoreCase).Match(month).Success: return "08";
                case var month when new Regex(@"(September|september|septiembre)", RegexOptions.IgnoreCase).Match(month).Success: return "09";
                case var month when new Regex(@"(October|oktober|octubre)", RegexOptions.IgnoreCase).Match(month).Success: return "10";
                case var month when new Regex(@"(November|november|noviembre)", RegexOptions.IgnoreCase).Match(month).Success: return "11";
                case var month when new Regex(@"(December|december|diciembre)", RegexOptions.IgnoreCase).Match(month).Success: return "12";
            }

            return s;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            var date = Regex.Match(s, @"(?<month>\d{2})[^0-9]*(?<day>\d{1,2})[^0-9]*(?<year>\d{4})");
            var day = date.Groups["day"].Value;
            if (date.Groups["day"].Length == 1)
                day = $"0{day}";
            dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + day;
            return dateNormalized;
        }
    }
}

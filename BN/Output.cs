﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static BN.Elements;

namespace BN
{
    public class Output
    {
        public static void ApplicationToFile(List<Applications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_App.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        if(record.CORN != null && record.CORN != "")
                        {
                            sf.WriteLine("CORN:\t" + record.CORN);
                            sf.WriteLine("CORA:\t" + record.CORA);
                            sf.WriteLine("CORC:\t" + record.CORC);
                        }
                        if (record.DESC != null && record.DESC.Count > 0)
                        {
                            for (int i = 0; i < record.DESC.Count; i++)
                            {
                                sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                                sf.WriteLine("DESC:\t" + record.DESC[i]);
                            }
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                        if(record.VIEC != null && record.VIEC.Count > 0)
                        {
                            for(int i = 0; i < record.VIEC.Count; i++)
                            {
                                sf.WriteLine("VIEC:\t" + record.VIEC[i]);
                            }
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

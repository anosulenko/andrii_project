﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static BN.Elements;

namespace BN
{
    class Processing : Elements
    {
        private static readonly string I210 = "[210]";
        private static readonly string I220 = "[220]";
        private static readonly string I510 = "[510]";
        private static readonly string I511 = "[511]";
        private static readonly string I531 = "[531]";
        private static readonly string I540 = "[540]";
        private static readonly string I730 = "[730]";
        private static readonly string I740 = "[740]";
        private static readonly string I750 = "[750]";

        public static DirectoryInfo processed;

        public static List<Applications> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if (elements != null && elements.Count > 0)
            {
                string[] splittedRecord = null;
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].Name.LocalName == "Page")
                    {
                        pgnr = elements[i].Attribute("number").Value;
                    }
                    var value = elements[i].Value.Replace("_", "").Trim();
                    if (value.StartsWith(I540))
                    {
                        var currentElement = new Applications();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);

                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Replace("_", "").Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.Replace("_", "").Trim().StartsWith(I540));

                        var tradeMark = Regex.Match(tmpRecordValue, @"Trade Mark Journal No. \d{3}/\d{4} \d{2} \p{L}+, \d{4}").Value;
                        if (!string.IsNullOrEmpty(tradeMark))
                        {
                            tmpRecordValue = tmpRecordValue.Replace(tradeMark, "");
                        }

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }

                        foreach(var record in splittedRecord)
                        {
                            if (record.StartsWith(I540))
                            {
                                currentElement.TMNM = record.Replace(I540, "").Replace("\n", " ").Trim();
                                currentElement.TMTY = "W";
                            }
                            if (record.StartsWith(I210))
                            {
                                currentElement.APNR = record.Replace(I210, "").Trim();
                                if (lastImageId != null)
                                {
                                    currentElement.TMTY = "M";

                                    string ext = Path.GetExtension(Images[lastImageId]);
                                    string fileName = Path.Combine(BN_main.PathToTetml.FullName, Images[lastImageId]);
                                    processed = Directory.CreateDirectory(Path.Combine(BN_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(BN_main.currentFile.FullName) + "\\App"));
                                    if (File.Exists(fileName))
                                        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR) + ext); }
                                        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName);}

                                    else currentElement.TMTY = "W";
                                }
                            }
                            if (record.StartsWith(I220))
                            {
                                var match = Regex.Match(record, @"\p{L}+");
                                var date = Methods.ToDate(match.Value);
                                date = record.Replace(I220, "").Replace(match.Value, date).Trim();
                                if(!Regex.IsMatch(date.Substring(0, 2), @"[0-9]{2}"))
                                {
                                    date = "0" + date;
                                }
                                date = Methods.DateNormalize(date);
                                currentElement.APDA = date;
                            }
                            if (record.StartsWith(I531))
                            {
                                var items = Methods.ToVIEC(record.Replace(I531, "").Trim());
                                var list = new List<string>();
                                foreach(var viec in items)
                                {
                                    list.Add(viec);
                                }
                                currentElement.VIEC = list;
                            }
                            if (record.StartsWith(I730))
                            {
                                var OWNN = record.Split('\n').First(); ;
                                var OWNA = record.Replace("\n", " ").Replace(OWNN, "");
                                var OWNC = Methods.ToOWNC(OWNA);
                                currentElement.OWNN = OWNN.Replace(I730, "").Trim();
                                currentElement.OWNA = OWNA.Trim();
                                currentElement.OWNC = OWNC;
                            }
                            if (record.StartsWith(I740))
                            {
                                currentElement.CORN = record.Replace(I740, "").Trim();
                            }
                            if (record.StartsWith(I750))
                            {
                                currentElement.CORC = Methods.ToOWNC(record.Replace(I750, "").Trim());
                                currentElement.CORA = record.Replace(I750, "").Replace("\n", " ").Trim();
                            }
                            if (record.StartsWith(I511))
                            {
                                var text = record.Replace(I511, "").Trim();
                                var split = text.Split("Cl.");
                                var classes = new List<string>();
                                var descs = new List<string>();

                                foreach(var item in split)
                                {
                                    if(item != "")
                                    {
                                        text = "";
                                        text = $"Cl.{item}".Replace("\n", " ");
                                        var pattern = new Regex(@"(?<CLAS>Cl\.\d{2}\.)\s*(?<DESC>.+)");
                                        var match = pattern.Match(text);
                                        if (match.Success)
                                        {
                                            classes.Add(match.Groups["CLAS"].Value.Replace("Cl", "").Replace(".", "").Trim());
                                            descs.Add(match.Groups["DESC"].Value);
                                        }
                                    }
                                }

                                currentElement.DESC = descs;
                                currentElement.CLAS = classes;
                            }
                            currentElement.PGNR = pgnr;
                        }
                    }
                }
            }
            return elementsOut;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace BT
{
    class Processing : Elements
    {
        private static double tempVal;
        private static string I540Val;

        private static readonly string I210 = "(210)";
        private static readonly string I220 = "(220)";
        private static readonly string I731 = "(731)";
        private static readonly string I750 = "(750)";
        private static readonly string I526 = "(526)";
        private static readonly string I571 = "(571)";
        private static readonly string I310 = "(310)";
        private static readonly string I320 = "(320)";
        private static readonly string I330 = "(330)";
        private static readonly string I511 = "(511)";
        private static readonly string I540 = "(540)";

        public static DirectoryInfo processed;

        public static List<Applications> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if(elements != null && elements.Count > 0)
            {
                string[] splittedRecord = null;
                string tmpRecordValue;
                int tmpInc;
                int pgnr = 1;
                for (int i = 0; i < elements.Count; i++)
                {
                    if (elements[i].Name.LocalName == "Page")
                    {
                        pgnr = Methods.ToInt(elements[i].Attribute("number").Value);
                    }
                    var value = elements[i].Value;
                    if (value.StartsWith(I210))
                    {
                        var currentElement = new Applications();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if(elements[tmpInc].Name.LocalName != "Page")
                            {
                                if(elements[tmpInc].Name.LocalName == "PlacedImage")
                                {
                                    ++tmpInc;
                                    continue;
                                }

                                if (elements[tmpInc].Parent.ToString().Contains("Text"))
                                {
                                    try
                                    {
                                        tempVal = double.Parse(elements[tmpInc].Parent.Attribute("llx").Value, new CultureInfo("en-US"));
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine(e);
                                        throw;
                                    }
                                }

                                if (tempVal < 400)
                                {
                                    tmpRecordValue += elements[tmpInc].Value + "\n";
                                    ++tmpInc;
                                }
                                else
                                {
                                    I540Val = elements[tmpInc].Value;
                                    ++tmpInc;
                                }
                            }
                            else
                            {
                                ++tmpInc;
                            }
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.StartsWith(I210));

                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }

                        foreach (var record in splittedRecord)
                        {
                            currentElement.TMNM = I540Val.Replace("\n", " ").Replace(I540, "").Replace(":", "").Trim();
                            currentElement.TMTY = "M";

                            if (record.StartsWith(I210))
                            {
                                var input = record
                                    .Replace("Application", "")
                                    .Replace("Number", "")
                                    .Replace(I210, "")
                                    .Replace(":", "")
                                    .Trim();
                                currentElement.APNR = input.Replace(I210, "").Trim().Replace("/", "_");
                                processed = Directory.CreateDirectory(Path.Combine(BT_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(BT_main.currentFile.FullName) + "\\Application"));
                            }

                            if (record.StartsWith(I220))
                            {
                                var match = Regex.Match(record, "Application Date ?:?");
                                currentElement.APDA = Methods.DateNormalize(record.Replace(I220, "").Replace(match.Value, "").Trim());
                            }
                            if (record.StartsWith(I731))
                            {
                                var input = record;
                                input = input.Replace(I731, "");
                                input = input.Replace("Applicant", "");
                                input = input.Replace("Name/Address", "");
                                input = input.Replace(":", "");
                                input = input.Replace("\n\n", "\n");
                                input = input.Trim();

                                

                                if (!string.IsNullOrEmpty(input))
                                {
                                    var strings = input.Split('\n');
                                    currentElement.OWNN = strings[0];
                                    currentElement.OWNC = Methods.ToOWNC(record.Trim(',').Split(",").Last().Trim().Replace("\n", " "));
                                    currentElement.OWNA = input.Replace("\n", " ").Replace(currentElement.OWNN, "").Replace(currentElement.OWNC, "").Trim().Trim(',').Trim();
                                }
                            }
                            if (record.StartsWith(I750))
                            {
                                var input = record.Replace(I750, "")
                                    .Replace("Representative", "")
                                    .Replace("Name/Address", "")
                                    .Replace(":", "").Trim();
                                if (!string.IsNullOrEmpty(input))
                                {
                                    var strings = input.Split('\n');
                                    currentElement.CORN = strings[0].Trim();
                                    currentElement.CORA = input
                                        .Replace(currentElement.CORN, "")
                                        .Replace("\n", " ")
                                        .Trim();
                                }
                            }
                            if (record.StartsWith(I310 + I320 + I330))
                            {
                                var input = record.Replace(I310, "")
                                    .Replace(I320, "")
                                    .Replace(I330, "")
                                    .Replace(":", "")
                                    .Trim();
                                if (!string.IsNullOrEmpty(input))
                                {
                                    var strings = input.Split(' ');
                                    currentElement.PRIN = strings[0];
                                    currentElement.PRID = Methods.DateNormalize(strings[1]);
                                    currentElement.PRIC = strings[2];
                                }
                            }
                            if (record.StartsWith(I511))
                            {
                                List<string> classes = new List<string>();
                                List<string> descs = new List<string>();
                                var match = Regex.Match(record, "Goods/Services ?:?");
                                var input = record
                                    .Replace(I511, "")
                                    .Replace(match.Value, "")
                                    .Trim();
                                var matches = Regex.Matches(record, @"[0-9]{1,2} .*?\.", RegexOptions.Singleline);

                                foreach (Match item in matches)
                                {
                                    var @class = Regex.Match(item.Value, "[0-9]{1,2} ");
                                    classes.Add(@class?.Value?.Trim());
                                    descs.Add(item.Value.Replace(@class.Value, ""));
                                }

                                currentElement.CLAS = classes;
                                currentElement.DESC = descs;
                            }
                            if (record.StartsWith(I540))
                            {
                                currentElement.TMNM = record.Replace(I540, "").Trim();
                            }
                            currentElement.PGNR = $"{pgnr}";
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

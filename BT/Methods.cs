﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BT
{
    public class Methods
    {
        public static string[] RecSplit(string recString)
        {
            string[] splittedRecord = null;
            string tempStrC = recString.Trim();

            if (tempStrC != "")
            {
                Regex regexPatOne = new Regex(@"\(\d{3}\)(\(\d{3}\)\(\d{3}\))?", RegexOptions.IgnoreCase);
                MatchCollection matchesClass = regexPatOne.Matches(tempStrC);
                if (matchesClass.Count > 0)
                {
                    foreach (Match matchC in matchesClass)
                    {
                        tempStrC = tempStrC.Replace(matchC.Value, "***" + matchC.Value);
                    }
                }

                splittedRecord = tempStrC.Split(new string[] { "***" }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();
            }
            return splittedRecord;
        }

        public static string DateNormalize(string s)
        {
            string dateNormalized = s;
            if (Regex.IsMatch(s, @"\d{2}\/*\-*\.*\d{2}\/*\-*\.*\d{4}"))
            {
                var date = Regex.Match(s, @"(?<day>\d{2})\/*\-*\.*(?<month>\d{2})\/*\-*\.*(?<year>\d{4})");
                dateNormalized = date.Groups["year"].Value + date.Groups["month"].Value + date.Groups["day"].Value;
            }
            return dateNormalized;
        }


        public static int ToInt(string str)
        {
            str = str.Trim(' ', '\n', '\r');

            int.TryParse(str, NumberStyles.None, CultureInfo.InvariantCulture, out var result);

            return result;
        }

        public static string ToOWNC(string s)
        {
            switch (s)
            {
                case var country when new Regex(@"(African Intellectual Property Organization)|", RegexOptions.IgnoreCase).IsMatch(country): return "OA";
                case var country when new Regex(@"Albania", RegexOptions.IgnoreCase).IsMatch(country): return "AL";
                case var country when new Regex(@"Algeria", RegexOptions.IgnoreCase).IsMatch(country): return "DZ";
                case var country when new Regex(@"Andorra", RegexOptions.IgnoreCase).IsMatch(country): return "AD";
                case var country when new Regex(@"Angola", RegexOptions.IgnoreCase).IsMatch(country): return "AO";
                case var country when new Regex(@"Argentina", RegexOptions.IgnoreCase).IsMatch(country): return "AR";
                case var country when new Regex(@"Armenia", RegexOptions.IgnoreCase).IsMatch(country): return "AM";
                case var country when new Regex(@"AsiaPacific Region", RegexOptions.IgnoreCase).IsMatch(country): return "AP";
                case var country when new Regex(@"Australia", RegexOptions.IgnoreCase).IsMatch(country): return "AU";
                case var country when new Regex(@"Austria", RegexOptions.IgnoreCase).IsMatch(country): return "AT";
                case var country when new Regex(@"Azerbeidzjan", RegexOptions.IgnoreCase).IsMatch(country): return "AZ";
                case var country when new Regex(@"Bahamas", RegexOptions.IgnoreCase).IsMatch(country): return "BS";
                case var country when new Regex(@"Bangladesh", RegexOptions.IgnoreCase).IsMatch(country): return "BD";
                case var country when new Regex(@"Barbados", RegexOptions.IgnoreCase).IsMatch(country): return "BB";
                case var country when new Regex(@"Belarus", RegexOptions.IgnoreCase).IsMatch(country): return "BY";
                case var country when new Regex(@"Belgium", RegexOptions.IgnoreCase).IsMatch(country): return "BE";
                case var country when new Regex(@"Belize", RegexOptions.IgnoreCase).IsMatch(country): return "BZ";
                case var country when new Regex(@"Bhutan", RegexOptions.IgnoreCase).IsMatch(country): return "BT";
                case var country when new Regex(@"Bolivia", RegexOptions.IgnoreCase).IsMatch(country): return "BO";
                case var country when new Regex(@"Bosnia And Herzegovina", RegexOptions.IgnoreCase).IsMatch(country): return "BA";
                case var country when new Regex(@"Botswana", RegexOptions.IgnoreCase).IsMatch(country): return "BW";
                case var country when new Regex(@"Brasil", RegexOptions.IgnoreCase).IsMatch(country): return "BR";
                case var country when new Regex(@"Brunei Darussalam", RegexOptions.IgnoreCase).IsMatch(country): return "BN";
                case var country when new Regex(@"Bulgaria", RegexOptions.IgnoreCase).IsMatch(country): return "BG";
                case var country when new Regex(@"Canada", RegexOptions.IgnoreCase).IsMatch(country): return "CA";
                case var country when new Regex(@"Cape Verde", RegexOptions.IgnoreCase).IsMatch(country): return "CV";
                case var country when new Regex(@"Cayman Islands", RegexOptions.IgnoreCase).IsMatch(country): return "KY";
                case var country when new Regex(@"Chili", RegexOptions.IgnoreCase).IsMatch(country): return "CL";
                case var country when new Regex(@"China", RegexOptions.IgnoreCase).IsMatch(country): return "CN";
                case var country when new Regex(@"Colombia", RegexOptions.IgnoreCase).IsMatch(country): return "CO";
                case var country when new Regex(@"CostaRica", RegexOptions.IgnoreCase).IsMatch(country): return "CR";
                case var country when new Regex(@"Croatia", RegexOptions.IgnoreCase).IsMatch(country): return "HR";
                case var country when new Regex(@"Cuba", RegexOptions.IgnoreCase).IsMatch(country): return "CU";
                case var country when new Regex(@"Czech", RegexOptions.IgnoreCase).IsMatch(country): return "CZ";
                case var country when new Regex(@"Czechoslovakia", RegexOptions.IgnoreCase).IsMatch(country): return "CS";
                case var country when new Regex(@"Denmark", RegexOptions.IgnoreCase).IsMatch(country): return "DK";
                case var country when new Regex(@"Djibouti", RegexOptions.IgnoreCase).IsMatch(country): return "DJ";
                case var country when new Regex(@"Dominica", RegexOptions.IgnoreCase).IsMatch(country): return "DM";
                case var country when new Regex(@"Dominican Republic", RegexOptions.IgnoreCase).IsMatch(country): return "DO";
                case var country when new Regex(@"Eapo", RegexOptions.IgnoreCase).IsMatch(country): return "EA";
                case var country when new Regex(@"Ecuador", RegexOptions.IgnoreCase).IsMatch(country): return "EC";
                case var country when new Regex(@"Egypt", RegexOptions.IgnoreCase).IsMatch(country): return "EG";
                case var country when new Regex(@"El Salvador", RegexOptions.IgnoreCase).IsMatch(country): return "SV";
                case var country when new Regex(@"Epo", RegexOptions.IgnoreCase).IsMatch(country): return "EP";
                case var country when new Regex(@"Estonia", RegexOptions.IgnoreCase).IsMatch(country): return "EE";
                case var country when new Regex(@"Ethiopia", RegexOptions.IgnoreCase).IsMatch(country): return "ET";
                case var country when new Regex(@"Fiji", RegexOptions.IgnoreCase).IsMatch(country): return "FJ";
                case var country when new Regex(@"Finland", RegexOptions.IgnoreCase).IsMatch(country): return "FI";
                case var country when new Regex(@"France", RegexOptions.IgnoreCase).IsMatch(country): return "FR";
                case var country when new Regex(@"German Democratic Republic", RegexOptions.IgnoreCase).IsMatch(country): return "DD";
                case var country when new Regex(@"Germany", RegexOptions.IgnoreCase).IsMatch(country): return "DE";
                case var country when new Regex(@"Ghana", RegexOptions.IgnoreCase).IsMatch(country): return "GH";
                case var country when new Regex(@"GreatBritain", RegexOptions.IgnoreCase).IsMatch(country): return "GB";
                case var country when new Regex(@"Greece", RegexOptions.IgnoreCase).IsMatch(country): return "GR";
                case var country when new Regex(@"Guatemala", RegexOptions.IgnoreCase).IsMatch(country): return "GT";
                case var country when new Regex(@"Gulf Cooperation Council", RegexOptions.IgnoreCase).IsMatch(country): return "GC";
                case var country when new Regex(@"Guyana", RegexOptions.IgnoreCase).IsMatch(country): return "GY";
                case var country when new Regex(@"Honduras", RegexOptions.IgnoreCase).IsMatch(country): return "HN";
                case var country when new Regex(@"HongKong", RegexOptions.IgnoreCase).IsMatch(country): return "HK";
                case var country when new Regex(@"Hungary", RegexOptions.IgnoreCase).IsMatch(country): return "HU";
                case var country when new Regex(@"Iceland", RegexOptions.IgnoreCase).IsMatch(country): return "IS";
                case var country when new Regex(@"India", RegexOptions.IgnoreCase).IsMatch(country): return "IN";
                case var country when new Regex(@"Indonesia", RegexOptions.IgnoreCase).IsMatch(country): return "ID";
                case var country when new Regex(@"Iraq", RegexOptions.IgnoreCase).IsMatch(country): return "IQ";
                case var country when new Regex(@"Ireland", RegexOptions.IgnoreCase).IsMatch(country): return "IE";
                case var country when new Regex(@"Israel", RegexOptions.IgnoreCase).IsMatch(country): return "IL";
                case var country when new Regex(@"Italy", RegexOptions.IgnoreCase).IsMatch(country): return "IT";
                case var country when new Regex(@"Japan", RegexOptions.IgnoreCase).IsMatch(country): return "JP";
                case var country when new Regex(@"Jordan", RegexOptions.IgnoreCase).IsMatch(country): return "JO";
                case var country when new Regex(@"Kenya", RegexOptions.IgnoreCase).IsMatch(country): return "KE";
                case var country when new Regex(@"Korea", RegexOptions.IgnoreCase).IsMatch(country): return "KR";
                case var country when new Regex(@"Kyrgyzstan", RegexOptions.IgnoreCase).IsMatch(country): return "KG";
                case var country when new Regex(@"Latvia", RegexOptions.IgnoreCase).IsMatch(country): return "LV";
                case var country when new Regex(@"Lebanon", RegexOptions.IgnoreCase).IsMatch(country): return "LB";
                case var country when new Regex(@"Lithuania", RegexOptions.IgnoreCase).IsMatch(country): return "LT";
                case var country when new Regex(@"Luxemburg", RegexOptions.IgnoreCase).IsMatch(country): return "LU";
                case var country when new Regex(@"Macedonia", RegexOptions.IgnoreCase).IsMatch(country): return "MK";
                case var country when new Regex(@"Madagascar", RegexOptions.IgnoreCase).IsMatch(country): return "MG";
                case var country when new Regex(@"Malaysia", RegexOptions.IgnoreCase).IsMatch(country): return "MY";
                case var country when new Regex(@"Mauritius", RegexOptions.IgnoreCase).IsMatch(country): return "MU";
                case var country when new Regex(@"Mexico", RegexOptions.IgnoreCase).IsMatch(country): return "MX";
                case var country when new Regex(@"Monaco", RegexOptions.IgnoreCase).IsMatch(country): return "MC";
                case var country when new Regex(@"Mongolia", RegexOptions.IgnoreCase).IsMatch(country): return "MN";
                case var country when new Regex(@"Montenegro", RegexOptions.IgnoreCase).IsMatch(country): return "ME";
                case var country when new Regex(@"Montserrat", RegexOptions.IgnoreCase).IsMatch(country): return "MS";
                case var country when new Regex(@"Morocco", RegexOptions.IgnoreCase).IsMatch(country): return "MA";
                case var country when new Regex(@"Mozambique", RegexOptions.IgnoreCase).IsMatch(country): return "MZ";
                case var country when new Regex(@"Myanmar Burma", RegexOptions.IgnoreCase).IsMatch(country): return "MM";
                case var country when new Regex(@"Moldova", RegexOptions.IgnoreCase).IsMatch(country): return "MD";
                case var country when new Regex(@"Netherlands", RegexOptions.IgnoreCase).IsMatch(country): return "NL";
                case var country when new Regex(@"New Zealand", RegexOptions.IgnoreCase).IsMatch(country): return "NZ";
                case var country when new Regex(@"Norway", RegexOptions.IgnoreCase).IsMatch(country): return "NO";
                case var country when new Regex(@"Oman", RegexOptions.IgnoreCase).IsMatch(country): return "OM";
                case var country when new Regex(@"Pakistan", RegexOptions.IgnoreCase).IsMatch(country): return "PK";
                case var country when new Regex(@"Palestina", RegexOptions.IgnoreCase).IsMatch(country): return "PS";
                case var country when new Regex(@"Panama", RegexOptions.IgnoreCase).IsMatch(country): return "PA";
                case var country when new Regex(@"Papua New Guinea", RegexOptions.IgnoreCase).IsMatch(country): return "PG";
                case var country when new Regex(@"Paraguay", RegexOptions.IgnoreCase).IsMatch(country): return "PY";
                case var country when new Regex(@"Philippines", RegexOptions.IgnoreCase).IsMatch(country): return "PH";
                case var country when new Regex(@"Poland", RegexOptions.IgnoreCase).IsMatch(country): return "PL";
                case var country when new Regex(@"Portugal", RegexOptions.IgnoreCase).IsMatch(country): return "PT";
                case var country when new Regex(@"Qatar", RegexOptions.IgnoreCase).IsMatch(country): return "QA";
                case var country when new Regex(@"Republic Of Kosovo", RegexOptions.IgnoreCase).IsMatch(country): return "XK";
                case var country when new Regex(@"Romania", RegexOptions.IgnoreCase).IsMatch(country): return "RO";
                case var country when new Regex(@"Russia", RegexOptions.IgnoreCase).IsMatch(country): return "RU";
                case var country when new Regex(@"Rwanda", RegexOptions.IgnoreCase).IsMatch(country): return "RW";
                case var country when new Regex(@"Saint Vincen tAnd The Grenadines", RegexOptions.IgnoreCase).IsMatch(country): return "VC";
                case var country when new Regex(@"San Marino", RegexOptions.IgnoreCase).IsMatch(country): return "SM";
                case var country when new Regex(@"Saudi Arabia", RegexOptions.IgnoreCase).IsMatch(country): return "SA";
                case var country when new Regex(@"Singapore", RegexOptions.IgnoreCase).IsMatch(country): return "SG";
                case var country when new Regex(@"Slovakia", RegexOptions.IgnoreCase).IsMatch(country): return "SK";
                case var country when new Regex(@"Slovenia", RegexOptions.IgnoreCase).IsMatch(country): return "SI";
                case var country when new Regex(@"South Africa", RegexOptions.IgnoreCase).IsMatch(country): return "ZA";
                case var country when new Regex(@"Soviet Union", RegexOptions.IgnoreCase).IsMatch(country): return "SU";
                case var country when new Regex(@"Spain", RegexOptions.IgnoreCase).IsMatch(country): return "ES";
                case var country when new Regex(@"Sri Lanka", RegexOptions.IgnoreCase).IsMatch(country): return "LK";
                case var country when new Regex(@"Sudan", RegexOptions.IgnoreCase).IsMatch(country): return "SD";
                case var country when new Regex(@"Sweden", RegexOptions.IgnoreCase).IsMatch(country): return "SE";
                case var country when new Regex(@"Switzerland", RegexOptions.IgnoreCase).IsMatch(country): return "CH";
                case var country when new Regex(@"Syria", RegexOptions.IgnoreCase).IsMatch(country): return "SY";
                case var country when new Regex(@"Taiwan", RegexOptions.IgnoreCase).IsMatch(country): return "TW";
                case var country when new Regex(@"Tajikistan", RegexOptions.IgnoreCase).IsMatch(country): return "TJ";
                case var country when new Regex(@"Tanzania", RegexOptions.IgnoreCase).IsMatch(country): return "TZ";
                case var country when new Regex(@"Thailand", RegexOptions.IgnoreCase).IsMatch(country): return "TH";
                case var country when new Regex(@"Trinidad And Tobago", RegexOptions.IgnoreCase).IsMatch(country): return "TT";
                case var country when new Regex(@"Tunisia", RegexOptions.IgnoreCase).IsMatch(country): return "TN";
                case var country when new Regex(@"Turkey", RegexOptions.IgnoreCase).IsMatch(country): return "TR";
                case var country when new Regex(@"Turkmenistan", RegexOptions.IgnoreCase).IsMatch(country): return "TM";
                case var country when new Regex(@"Ukraine", RegexOptions.IgnoreCase).IsMatch(country): return "UA";
                case var country when new Regex(@"United Arab Emirates", RegexOptions.IgnoreCase).IsMatch(country): return "AE";
                case var country when new Regex(@"United Kingdom", RegexOptions.IgnoreCase).IsMatch(country): return "GB";
                case var country when new Regex(@"United States", RegexOptions.IgnoreCase).IsMatch(country): return "US";
                case var country when new Regex(@"Uruguay", RegexOptions.IgnoreCase).IsMatch(country): return "UY";
                case var country when new Regex(@"Uzbekistan", RegexOptions.IgnoreCase).IsMatch(country): return "UZ";
                case var country when new Regex(@"Venezuela", RegexOptions.IgnoreCase).IsMatch(country): return "VE";
                case var country when new Regex(@"Viet nam", RegexOptions.IgnoreCase).IsMatch(country): return "VN";
                case var country when new Regex(@"Wipo", RegexOptions.IgnoreCase).IsMatch(country): return "WO";
                case var country when new Regex(@"Yemen", RegexOptions.IgnoreCase).IsMatch(country): return "YE";
                case var country when new Regex(@"Zambia", RegexOptions.IgnoreCase).IsMatch(country): return "ZM";
                case var country when new Regex(@"Zimbabwe", RegexOptions.IgnoreCase).IsMatch(country): return "ZW";
                case var country when new Regex(@"Cyprus", RegexOptions.IgnoreCase).IsMatch(country): return "CY";
            }

            return s;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using static BT.Elements;

namespace BT
{
    public class Output
    {
        public static void ApplicationToFile(List<Applications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + "_App.txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("APDA:\t" + record.APDA);
                        sf.WriteLine("TMNM:\t" + record.TMNM);
                        sf.WriteLine("TMTY:\t" + record.TMTY);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        sf.WriteLine("OWNA:\t" + record.OWNA);
                        sf.WriteLine("OWNC:\t" + record.OWNC);
                        sf.WriteLine("CORN:\t" + record.CORN);
                        sf.WriteLine("CORA:\t" + record.CORA);
                        sf.WriteLine("CORC:\t" + record.CORC);
                        if (!string.IsNullOrEmpty(record.PRIC))
                        {
                            sf.WriteLine("PRIC:\t" + record.PRIC);
                            sf.WriteLine("PRIN:\t" + record.PRIN);
                            sf.WriteLine("PRID:\t" + record.PRID);
                        }

                        for(int i = 0; i < record.DESC.Count; i++)
                        {
                            sf.WriteLine("CLAS:\t" + record.CLAS[i]);
                            sf.WriteLine("DESC:\t" + record.DESC[i]);
                        }
                        sf.WriteLine("PGNR:\t" + record.PGNR);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

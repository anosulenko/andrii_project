﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_TM
{
    public struct ClassDesc
    {
        public string CLAS { get; set; }
        public string DESC { get; set; }
    }
    public class ElementsToOutputApplications
    {
        public string APNR { get; set; }
        public string RGNR { get; set; }
        public string RGDA { get; set; }
        public string EXDA { get; set; }
        public string TMTY { get; set; }
        public string OWNN { get; set; }
        public string OWNC { get; set; }
        public string OWNA { get; set; }
        public string PrioNumber { get; set; }
        public string PrioDate { get; set; }
        public string PrioCountry { get; set; }
        public List<ClassDesc> ListClassDesc { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MA_TM
{
    public class Output
    {
        public static void ApplicationsToFile(List<ElementsToOutputApplications> output, DirectoryInfo pathProcessed)
        {
            var path = Path.Combine(pathProcessed.FullName, Directory.GetParent(pathProcessed.FullName).Name + ".txt");
            var sf = new StreamWriter(path);
            if (output != null)
            {
                foreach (var record in output)
                {
                    try
                    {
                        sf.WriteLine("****");
                        sf.WriteLine("APNR:\t" + record.APNR);
                        sf.WriteLine("RGNR:\t" + record.RGNR);
                        sf.WriteLine("APDA:\t" + record.RGDA);
                        sf.WriteLine("EXDA:\t" + record.EXDA);
                        sf.WriteLine("TMNM:\t");
                        sf.WriteLine("TMTY:\tM");
                        if (record.PrioCountry != null) sf.WriteLine("PRIC:\t" + record.PrioCountry);
                        if (record.PrioNumber != null) sf.WriteLine("PRIN:\t" + record.PrioNumber);
                        if (record.PrioDate != null) sf.WriteLine("PRID:\t" + record.PrioDate);
                        sf.WriteLine("OWNN:\t" + record.OWNN);
                        if (record.OWNA != "") sf.WriteLine("OWNA:\t" + record.OWNA);
                        if (record.OWNC != "") sf.WriteLine("OWNC:\t" + record.OWNC);
                        for (int i = 0; i < record.ListClassDesc.Count(); i++)
                        {
                            sf.WriteLine("CLAS:\t" + record.ListClassDesc[i].CLAS);
                            sf.WriteLine("DESC:\t" + record.ListClassDesc[i].DESC);
                        }
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Error:\t" + "PDF:\t" + Directory.GetParent(pathProcessed.FullName).Name + "\tAPNR:\t" + record.APNR);
                    }
                }
            }
            sf.Flush();
            sf.Close();
        }
    }
}

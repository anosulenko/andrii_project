﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MA_TM
{
    class MA_main
    {
        public static DirectoryInfo directoryWithTetml = new DirectoryInfo(@"D:\_DFA_main\_Trademarks\MA\20191216");
        public static FileInfo currentFile = null;
        static void Main(string[] args)
        {
            List<XElement> appElements = null;

            var listOfFiles = new List<string>();

            foreach (FileInfo file in directoryWithTetml.GetFiles("*.tetml", SearchOption.AllDirectories))
            {
                listOfFiles.Add(file.FullName);
            }
            foreach (var file in listOfFiles)
            {
                currentFile = new FileInfo(file);
                string FileName = file;
                XElement tet = XElement.Load(FileName);
                /*Old gazett format*/
                appElements = tet.Descendants().Where(d => d.Name.LocalName == "Text" || d.Name.LocalName == "PlacedImage")
                    .SkipWhile(d => d.Value != "I. DEMANDES D'ENREGIS TREMENT DE MARQUE")
                    .Where(x => x.Value != "I. DEMANDES D'ENREGIS TREMENT DE MARQUE")
                    .ToList();
                /*Applications processing*/
                if (appElements != null && appElements.Count > 0)
                {
                    var processedRecords = ApplicationsProcessing.AppProcess(appElements);
                    Output.ApplicationsToFile(processedRecords, ApplicationsProcessing.processed);
                }
            }
        }
    }
}

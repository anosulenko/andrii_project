﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MA_TM
{
    public class ApplicationsProcessing
    {
        static readonly string I151 = "(151)"; //RGDA
        static readonly string I180 = "(180)"; //EXDA
        static readonly string I732 = "(732)";
        static readonly string I511 = "(511)";
        static readonly string I300 = "(300)";


        public static DirectoryInfo processed/* = Directory.CreateDirectory(Path.Combine(MA_main.directoryWithTetml.FullName, Path.GetFileNameWithoutExtension(MA_main.currentFile.FullName) + "\\App"))*/;
        public static List<ElementsToOutputApplications> AppProcess(List<XElement> elements)
        {
            processed = Directory.CreateDirectory(Path.Combine(MA_main.directoryWithTetml.FullName, Path.GetFileNameWithoutExtension(MA_main.currentFile.FullName) + "\\App"));
            List<ElementsToOutputApplications> elementsOut = new List<ElementsToOutputApplications>();
            if (elements != null && elements.Count > 0)
            {
                Regex pat = new Regex(@"^\d{6}$");
                string[] splittedRecord = null;
                string tmpRecordValue;
                int tmpInc;
                for (int i = 0; i < elements.Count; ++i)
                {
                    var value = elements[i].Value;
                    if (pat.Match(value).Success)
                    {
                        var currentElement = new ElementsToOutputApplications();
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            tmpRecordValue += elements[tmpInc].Value + "\n";
                            ++tmpInc;
                        } while (tmpInc < elements.Count() && !pat.Match(elements[tmpInc].Value).Success);
                        if (tmpRecordValue != null)
                        {
                            splittedRecord = Methods.RecSplit(tmpRecordValue);
                        }
                        foreach (var record in splittedRecord)
                        {
                            if (pat.Match(record).Success)
                            {
                                currentElement.APNR = pat.Match(record).Value.Trim();
                                currentElement.RGNR = pat.Match(record).Value.Trim();
                            }
                            if (record.StartsWith(I151))
                            {
                                currentElement.RGDA = Methods.DateNormalize(record.Replace(I151, "").Trim());
                            }
                            if (record.StartsWith(I180))
                            {
                                currentElement.EXDA = Methods.DateNormalize(record.Replace(I180, "").Trim());
                            }
                            if (record.StartsWith(I732))
                            {
                                var owner = Methods.OwnerProcess(record.Replace(I732, "").Trim());
                                if (owner.Name == null) Console.WriteLine("Eror in owner identification:\t" + currentElement.APNR);
                                currentElement.OWNN = owner.Name;
                                currentElement.OWNA = owner.Address;
                                currentElement.OWNC = owner.Country;
                            }
                            if (record.StartsWith(I511))
                            {
                                currentElement.ListClassDesc = Methods.DescClass(record.Replace(I511, "").Trim());
                            }
                            if (record.StartsWith(I300))
                            {
                                var tmp = record.Replace(I300, "").Trim();
                                var p = new Regex(@"(?<country>^[A-Z]{2})\s*,*\s*(?<date>\d{4}-\d{2}-\d{2}).*,(?<number>.*$)");
                                if (tmp != "")
                                {
                                    var rx = p.Match(tmp);
                                    if (rx.Success)
                                    {
                                        currentElement.PrioNumber = rx.Groups["number"].Value.Trim();
                                        currentElement.PrioDate = rx.Groups["date"].Value.Trim();
                                        currentElement.PrioCountry = rx.Groups["country"].Value.Trim();
                                    }
                                    else
                                    {
                                        Console.WriteLine("Priority identification error:\t" + currentElement.APNR);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return elementsOut;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KE_TM
{
    class Elements
    {
        public struct DescClass
        {
            public string CLAS { get; set; }
            public string DESC { get; set; }
        }
        public struct Priority
        {
            public string PRIC { get; set; }
            public string PRIN { get; set; }
            public string PRID { get; set; }
        }
        public struct Owner
        {
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
        }
        public struct Agent
        {
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
        }
        public struct Applications
        {
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string PGNR { get; set; }
            public Priority PRIO { get; set; }
            public List<DescClass> Descs { get; set; }
            public Agent AGENT { get; set; }
            public Owner OWNER { get; set; }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KE_TM
{
    class Process
    {
        public class ApplicationsProcessing
        {
            static readonly string I210 = "(210)"; //
            static readonly string I220 = "(220)"; //
            static readonly string I300 = "(300)"; //
            static readonly string I511 = "(511)"; //
            static readonly string I730 = "(730)"; //
            static readonly string I740 = "(740)"; //

            public static DirectoryInfo processed;
            public static List<Elements.Applications> Run(List<XElement> elements/*, Dictionary<string, string> Images*/)
            {
                processed = Directory.CreateDirectory(Path.Combine(KE_main.directoryWithTetml.FullName, Path.GetFileNameWithoutExtension(KE_main.currentFile.FullName)));
                List<Elements.Applications> elementsOut = new List<Elements.Applications>();
                if (elements != null && elements.Count > 0)
                {
                    string[] splittedRecord = null;
                    string currentPageNumber = null;
                    string tmpRecordValue;
                    int tmpInc;
                    for (int i = 0; i < elements.Count; ++i)
                    {
                        if (elements[i].Name.LocalName == "Page")
                        {
                            currentPageNumber = elements[i].Attribute("number").Value;
                        }
                        var value = elements[i].Value;
                        if (value.StartsWith(I210))
                        {
                            var currentElement = new Elements.Applications();
                            //string lastImageId = null;
                            tmpRecordValue = "";
                            string tradeMarkName = "*";
                            tmpInc = i;
                            do
                            {
                                //if (elements[tmpInc].Name.LocalName == "PlacedImage")
                                //{
                                //    lastImageId = elements[tmpInc].Attribute("image").Value;
                                //}
                                if (elements[tmpInc].Name.LocalName != "Page")
                                {
                                    tmpRecordValue += elements[tmpInc].Value + "\n";
                                }
                                if (elements[tmpInc].Name.LocalName == "Text" && Convert.ToInt32(double.Parse(elements[tmpInc].Parent.Parent.Attribute("llx").Value.Replace(".", ","))) > 360)
                                {
                                    tradeMarkName = elements[tmpInc].Value.Trim();
                                }
                                ++tmpInc;
                            } while (tmpInc < elements.Count() && !elements[tmpInc].Value.StartsWith(I210));
                            /*trademark name and image process*/
                            if (tradeMarkName != "*")
                            {
                                currentElement.TMNM = tradeMarkName;
                                currentElement.TMTY = "W";
                            }
                            /*end tmnm and image process*/

                            if (tmpRecordValue != null) splittedRecord = Methods.RecSplit(tmpRecordValue.Replace(tradeMarkName, ""));

                            foreach (var record in splittedRecord)
                            {
                                if (record.StartsWith(I210))
                                {
                                    currentElement.APNR = record.Replace(I210, "").Replace("/", "_").Replace(" ", "").Trim();
                                    currentElement.PGNR = currentPageNumber;
                                    //if (lastImageId != null)
                                    //{
                                    //    currentElement.TMTY = "M";
                                    //    string ext = Path.GetExtension(Images[lastImageId]);
                                    //    string fileName = Path.Combine(KE_main.directoryWithTetml.FullName, Images[lastImageId]);
                                    //    if (File.Exists(fileName))
                                    //        try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR) + ext); }
                                    //        catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                                    //    else Console.WriteLine("Cannot locate file " + fileName);
                                    //}
                                    //else
                                    //{
                                    //    Console.WriteLine("");
                                    //}
                                }
                                if (record.StartsWith(I220))
                                {
                                    currentElement.APDA = Methods.DateNormalize(record.Replace(I220, "").Trim());
                                }
                                if (record.StartsWith(I300))
                                {
                                    string tmpV = record.Replace(I300, "").Trim();
                                    if (tmpV != "None" && tmpV != "Non None")
                                    {
                                        currentElement.PRIO = Methods.PriorityProcess(record.Replace(I300, "").Trim());
                                    }
                                }
                                if (record.StartsWith(I511))
                                {
                                    currentElement.Descs = Methods.DescClassProcess(record.Replace(I511, "").Trim());
                                }
                                if (record.StartsWith(I730))
                                {
                                    try
                                    {
                                        currentElement.OWNER = Methods.OwnerProcess(record.Replace(I730, "").Trim());
                                    }
                                    catch (Exception)
                                    {
                                        Console.WriteLine("Owner identification error in:\t" + currentElement.APNR);
                                    }
                                }
                                if (record.StartsWith(I740))
                                {
                                    string tmpAgent = record.Replace(I740, "").Trim();
                                    if (tmpAgent != "None")
                                    {
                                        currentElement.AGENT = Methods.AgentProcess(tmpAgent);
                                    }
                                }
                            }
                            elementsOut.Add(currentElement);
                        }
                    }
                }
                return elementsOut;
            }
        }
    }
}

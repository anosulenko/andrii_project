﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace KE_TM
{
    class KE_main
    {
        public static DirectoryInfo directoryWithTetml = new DirectoryInfo(@"D:\_DFA_main\_Trademarks\KE\20191205");
        public static FileInfo currentFile = null;
        static void Main(string[] args)
        {
            List<XElement> appElements = null;
            //List<XElement> imageElements = null;

            var listOfFiles = new List<string>();

            foreach (FileInfo file in directoryWithTetml.GetFiles("*.tetml", SearchOption.AllDirectories))
            {
                listOfFiles.Add(file.FullName);
            }
            foreach (var file in listOfFiles)
            {
                currentFile = new FileInfo(file);
                string FileName = file;
                //Dictionary<string, string> Images = new Dictionary<string, string>();
                XElement tet = XElement.Load(FileName);
                /*Old gazett format*/
                //appElements = tet.Descendants().Where(d => d.Name.LocalName == "Text" || d.Name.LocalName == "PlacedImage").ToList();
                appElements = tet.Descendants().Where(d => d.Name.LocalName == "Text" /*|| d.Name.LocalName == "PlacedImage"*/ || d.Name.LocalName == "Page").ToList();
                //imageElements = tet.Descendants().Where(d => d.Name.LocalName == "Image").ToList();
                //foreach (var item in imageElements)
                //{
                //    var value = item.Attribute("id")?.Value;
                //    if (value != null)
                //    {
                //        Images.Add(value, item.Attribute("filename")?.Value);
                //    }
                //}
                /*Applications processing*/
                if (appElements != null && appElements.Count > 0)
                {
                    var processedRecords = Process.ApplicationsProcessing.Run(appElements/*, Images*/);
                    Methods.ExportApplications(processedRecords, Process.ApplicationsProcessing.processed);
                }
            }
        }
    }
}

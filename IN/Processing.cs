﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using static IN.Elements;

namespace IN
{
    public class Processing
    {
        public static DirectoryInfo processed;

        public static string Start = "Trade Marks Journal";
        public static List<Applications> Applications(List<XElement> elements, Dictionary<string, string> Images)
        {
            List<Applications> elementsOut = new List<Applications>();

            if (elements != null && elements.Count > 0)
            {
                List<string> splittedRecord = new List<string>();
                string tmpRecordValue;
                int tmpInc;
                string pgnr = "";
                int pagenumber = 1;
                for (int i = 0; i < elements.Count; i++)
                {
                    var value = elements[i].Value;
                    if (value.StartsWith(Start))
                    {
                        pgnr = $"{pagenumber++}";
                        var currentElement = new Applications();
                        string lastImageId = null;
                        elementsOut.Add(currentElement);
                        tmpRecordValue = "";
                        tmpInc = i;
                        do
                        {
                            if (elements[tmpInc].Name.LocalName == "PlacedImage")
                            {
                                lastImageId = elements[tmpInc].Attribute("image").Value;
                            }
                            if (!elements[tmpInc].Value.Contains("tetml"))
                            {
                                tmpRecordValue += elements[tmpInc].Value.Trim() + "\n";
                            }
                            ++tmpInc;
                        } while (tmpInc < elements.Count && !elements[tmpInc].Value.StartsWith(Start));

                        i = tmpInc -1;

                        var @class = Regex.Match(tmpRecordValue, @"Class\s*[0-9]{1,2}").Value; 
                        var dateAndNumber = Regex.Match(tmpRecordValue, @"[0-9]+\s\d{2}\/\d{2}\/\d{4}").Value;
                        var firstPartOfText = tmpRecordValue.Substring(tmpRecordValue.IndexOf(@class), tmpRecordValue.IndexOf(dateAndNumber) - tmpRecordValue.IndexOf(@class));
                        var usedSince = Regex.Match(tmpRecordValue, @"Used Since :\d{2}\/\d{2}\/\d{4}").Value;
                        var associated = Regex.Match(tmpRecordValue, @"To be associated with:\s*(\d+,?\s*)+").Value;
                        var image = firstPartOfText.Replace(@class, "").Trim();
                        var number = dateAndNumber.Split(' ').First();
                        var date = Methods.DateNormalize(dateAndNumber.Split(' ').Last());

                        currentElement.APDA = date;
                        currentElement.APNR = number;

                        if (!string.IsNullOrEmpty(image))
                        {
                            var priorityClaim = Regex.Match(image, @"Priority claim.*", RegexOptions.IgnoreCase);
                            if (priorityClaim.Success)
                            {
                                image = image.Replace(priorityClaim.Value, "").Trim();
                            }
                            currentElement.TMNM = image;
                            currentElement.TMTY = "W";
                        }
                        else
                            currentElement.TMTY = "M";

                        var indexOfTrading = tmpRecordValue.IndexOf("trading as ;");
                        var indexOfAddress = tmpRecordValue.IndexOf("Address for service in");
                        var indexOfPropose = tmpRecordValue.IndexOf("Proposed to be Used");
                        var proposed = Regex.Match(tmpRecordValue, "Proposed to be Used").Value;
                        var indexOfSince = tmpRecordValue.IndexOf(usedSince);
                        var secondPartOfText = "";
                        var thirdPartOfText = "";
                        var forthPartOfText = "";
                        if(indexOfTrading > 0 && (indexOfSince > 0 || indexOfPropose > 0))
                        {
                            if(indexOfSince > 0)
                            {
                                secondPartOfText = tmpRecordValue.Substring(indexOfTrading, indexOfSince - indexOfTrading);
                                forthPartOfText = tmpRecordValue.Substring(indexOfSince + usedSince.Length).Trim();
                                if (indexOfAddress > 0)
                                {
                                    thirdPartOfText = tmpRecordValue.Substring(indexOfAddress).Replace(forthPartOfText, "")
                                        .Replace("Address for service in India/Agents address:", "")
                                        .Replace("Address for service in India/Attorney address:", "")
                                        .Trim();
                                    secondPartOfText = secondPartOfText.Substring(0, secondPartOfText.IndexOf("Address for service in")).Replace("trading as ;", "");
                                    currentElement.OWNN = secondPartOfText.Split('\n').First();
                                    currentElement.OWNA = secondPartOfText.Replace(currentElement.OWNN, "").Replace("\n", " ").Trim();
                                    //currentElement.OWNC = 
                                    currentElement.CORN = thirdPartOfText.Split('\n').First();
                                    currentElement.CORA = thirdPartOfText.Replace(currentElement.CORN, "").Replace("\n", " ").Replace(usedSince, "").Trim();
                                }
                                else
                                {
                                    secondPartOfText = secondPartOfText.Replace("trading as ;", "").Trim();
                                    currentElement.OWNN = secondPartOfText.Split('\n').First();
                                    currentElement.OWNA = secondPartOfText.Replace(currentElement.OWNN, "").Replace("\n", " ").Trim();
                                    //currentElement.OWNC
                                }
                            }
                            else
                            {
                                secondPartOfText = tmpRecordValue.Substring(indexOfTrading, indexOfPropose - indexOfTrading).Replace("trading as ;", "").Trim();
                                forthPartOfText = tmpRecordValue.Substring(indexOfPropose + proposed.Length).Trim();
                                currentElement.OWNN = secondPartOfText.Split('\n').First().Trim();
                                currentElement.OWNA = secondPartOfText.Replace(currentElement.OWNN, "").Replace("\n", " ").Trim();
                            }
                        }
                        else if(indexOfAddress > 0 && (indexOfSince > 0 || indexOfPropose > 0))
                        {
                            if(indexOfSince > 0)
                            {
                                secondPartOfText = tmpRecordValue.Substring(tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length, indexOfAddress - (tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length));
                                thirdPartOfText = tmpRecordValue.Substring(indexOfAddress, indexOfSince - indexOfAddress)
                                    .Replace("Address for service in India/Agents address:", "")
                                    .Replace("Address for service in India/Attorney address:", "")
                                    .Trim();
                                forthPartOfText = tmpRecordValue.Substring(indexOfSince + usedSince.Length);
                            }
                            else if(indexOfPropose > 0)
                            {
                                secondPartOfText = tmpRecordValue.Substring(tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length, indexOfAddress - (tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length));
                                thirdPartOfText = tmpRecordValue.Substring(indexOfAddress, indexOfPropose - indexOfAddress)
                                    .Replace("Address for service in India/Agents address:", "")
                                    .Replace("Address for service in India/Attorney address:", "")
                                    .Trim();
                                forthPartOfText = tmpRecordValue.Substring(indexOfPropose + proposed.Length);
                            }

                            currentElement.OWNN = secondPartOfText.Trim().Split('\n').First();
                            currentElement.OWNA = secondPartOfText.Replace(currentElement.OWNN, "").Replace("\n", " ").Trim();
                            //currentElement.OWNC = 
                            currentElement.CORN = thirdPartOfText.Trim().Split('\n').First();
                            currentElement.CORA = thirdPartOfText.Replace(currentElement.CORN, "").Replace("\n", " ").Trim();
                            //currentElement.CORC = 
                        }
                        else if((indexOfPropose > 0 || indexOfSince > 0) &&  indexOfAddress < 1)
                        {
                            if(indexOfPropose > 0)
                            {
                                secondPartOfText = tmpRecordValue.Substring(tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length, indexOfPropose - (tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length)).Trim();
                                forthPartOfText = tmpRecordValue.Substring(indexOfPropose + proposed.Length).Trim();
                            }
                            else if(indexOfSince > 0)
                            {
                                secondPartOfText = tmpRecordValue.Substring(tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length, indexOfSince - (tmpRecordValue.IndexOf(dateAndNumber) + dateAndNumber.Length)).Trim();
                                forthPartOfText = tmpRecordValue.Substring(indexOfSince + proposed.Length).Trim();
                            }

                            currentElement.OWNN = secondPartOfText.Split('\n').First();
                            currentElement.OWNA = secondPartOfText.Replace(currentElement.OWNN, "").Replace("\n", " ").Trim();
                        }

                        var indexOfRegistration = forthPartOfText.IndexOf("REGISTRATION");
                        if (indexOfRegistration > 0)
                        {
                            forthPartOfText = forthPartOfText.Substring(0, indexOfRegistration).Trim();
                        }

                        if (!string.IsNullOrEmpty(associated))
                        {
                            List<string> asno = new List<string>();
                            var matches = Regex.Matches(associated, @"\d+");
                            foreach (Match match in matches)
                            {
                                if (!string.IsNullOrEmpty(match.Value))
                                {
                                    asno.Add(match.Value);
                                }

                            }
                            currentElement.ASNO = asno;
                            var desc = forthPartOfText.Trim().Substring(forthPartOfText.IndexOf(associated) + associated.Length);
                            var skip = desc.Split('\n').First();
                            desc = desc.Replace(skip, "").Trim();
                            currentElement.DESC = desc;
                        }
                        else
                        {
                            var matchDesc = Regex.Match(forthPartOfText, @"\p{Lu}{1}\p{Ll}+,?\s(\p{Ll}+,?\s*)+.?").Value;
                            if (!string.IsNullOrEmpty(matchDesc))
                            {
                                currentElement.DESC = matchDesc;
                            }
                            else
                            {
                                forthPartOfText = forthPartOfText.Trim();
                                var skip = forthPartOfText.Split('\n').First();
                                forthPartOfText = forthPartOfText.Replace(skip, "").Trim();
                                currentElement.DESC = forthPartOfText;
                            }
                        }

                        processed = Directory.CreateDirectory(Path.Combine(IN_main.PathToTetml.FullName, Path.GetFileNameWithoutExtension(IN_main.currentFile.FullName) + "\\Applications"));
                        if (lastImageId != null)
                        {
                            string ext = Path.GetExtension(Images[lastImageId]);
                            string fileName = Path.Combine(IN_main.currentFile.DirectoryName, Images[lastImageId]);
                            if (File.Exists(fileName))
                                try { File.Copy(fileName, Path.Combine(processed.FullName, currentElement.APNR) + ext); }
                                catch (Exception) { Console.WriteLine("Image already exist:\t" + fileName); }
                        }
                        currentElement.CLAS = @class.Replace("Class", "").Trim();
                        currentElement.PGNR = pgnr;
                        currentElement.DESC = currentElement.DESC.Replace("\n", " ").Trim();
                        if(currentElement.OWNN.Contains(" AS "))
                        {
                            currentElement.OWNN = currentElement.OWNN.Split(new string[] { " AS " }, StringSplitOptions.None).Last();
                        }
                        if (currentElement.CLAS.Length == 1)
                            currentElement.CLAS = $"0{currentElement.CLAS}";
                        var regex = Regex.Match(currentElement.OWNA, @"(manufacturers?(\s*&\s*|\s*and\s*|\s*\/\s*|,\s*)?(traders?|merchants?)?|merchants?(\s*&\s*|\s*and\s*|\s*\/\s*|,\s*)manufacturers?)", RegexOptions.IgnoreCase).Value;
                        if (!string.IsNullOrEmpty(regex))
                        {
                            currentElement.OWNA = currentElement.OWNA.Substring(0, currentElement.OWNA.IndexOf(regex));
                        }
                    }
                }
            }

            return elementsOut;
        }
    }
}

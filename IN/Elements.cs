﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IN
{
    public class Elements
    {
        public class Applications
        {
            public string PGNR { get; set; }
            public string CLAS { get; set; }
            public string TMNM { get; set; }
            public string TMTY { get; set; }
            public string APNR { get; set; }
            public string APDA { get; set; }
            public string OWNN { get; set; }
            public string OWNA { get; set; }
            public string OWNC { get; set; }
            public string CORN { get; set; }
            public string CORA { get; set; }
            public string CORC { get; set; }
            public string DESC { get; set; }
            public List<string> ASNO { get; set; }
        }
    }
}
